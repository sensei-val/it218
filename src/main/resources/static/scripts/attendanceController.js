app.controller('clockController', function($scope, $interval) {

	//Show Current Date and Time	
	var tick = function() {
		$scope.currentDate = Date.now();
		$scope.currentTime = Date.now();
	};
	tick();
	$interval(tick, 1000);
});


app.controller('attendanceController', function($scope, $http) {
	
	var currentEmployeeId = localStorage.getItem("currentUserId");
	$scope.employeeName = localStorage.getItem("currentUserName");
	
	//Check button enabled status
	$http({
		method : "GET",
		url : "/attendance/todayAttendanceId/" + currentEmployeeId,
	}).then(function success(response) {
		if (response.data) {
			$scope.timeInKey = response.data;
			$("#timeInBtn").attr("disabled", true);
			$("#timeOutBtn").removeAttr("disabled");
			$("#requestPassBtn").removeAttr("disabled");			
		} else {
			$("#timeInBtn").removeAttr("disabled");
			$("#timeOutBtn").attr("disabled", true);
			$("#requestPassBtn").attr("disabled", true);
		}
	}, function error(response) {
		alert(response.data.error + " : " + response.data.message);
	});
	
		
	//REFRESH REQUEST PASS STATUS SUMMARY	
	$scope.refresh = function() {
				
		$http({
			method : "GET",
			url : "/requests/today/" + currentEmployeeId
		}).then(function success(response) {
			$scope.requests =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	$scope.refresh();
	
	//Send a Request to go out Pass
	$scope.requestPass = function() {
				
		$http({
			method : "POST",
			url : "/requests",
			params : { "requestorId" : currentEmployeeId } 
		}).then(function success(response) {
			$scope.refresh();
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	
	//Lock in TIME IN Record
	$scope.timeIn = function() {
		
		var attendanceData = {
			"employee" : {
				"id" : currentEmployeeId
			}
		};		
		$http({
			method : "POST",
			url : "/attendance/timeIn",
			data : JSON.stringify(attendanceData)
		}).then(function success(response) {
			$scope.timeInKey = response.data.id;
			$("#timeInBtn").attr("disabled", true);
			$("#timeOutBtn").removeAttr("disabled");
			$("#requestPassBtn").removeAttr("disabled");
			$scope.refresh();
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	}
	
	//Lock in TIME OUT Record
	$scope.timeOut = function() {
		
		var attendanceData = {
			"id" : $scope.timeInKey,
			"employee" : {
				"id" : currentEmployeeId
			}
		};
		$http({
			method : "PUT",
			url : "/attendance/timeOut",
			data : JSON.stringify(attendanceData)
		}).then(function success(response) {
			$scope.timeInKey = response.data.id;
			$("#timeInBtn").removeAttr("disabled");
			$("#timeOutBtn").attr("disabled", true);
			$("#requestPassBtn").attr("disabled", true);
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	}
	
});