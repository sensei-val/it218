
app.controller('passesController', function($scope, $http, $timeout) {
	
	var currentEmployeeId = localStorage.getItem("currentUserId");
	$timeout(function () {
        if (localStorage.getItem("currentUserRole") == "Department Manager") {
    		$scope.isShownFlag = true;
    	} else {
    		$scope.isShownFlag = false;
    	}
    }, 1000);
	
	//INITIAL LOAD
	$scope.initialLoad = function() {
		$http({
			method : "GET",
			url : "/departments"
		}).then(function success(response) {
			$scope.departmentsList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
		
		$http({
			method : "GET",
			url : "/teams"
		}).then(function success(response) {
			$scope.teamsList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	}
	$scope.initialLoad();
	
	//SEARCH FUNCTION
	$scope.search = function() {
			
		$http({
			method : "GET",
			url : "/employees/search",
			params : { 
				"employeeIdName": $scope.employeeInfoModel,
				"deptId" : $scope.departmentModel,
				"teamId" : $scope.teamModel
			}
		}).then(function success(response) {
			$scope.employeeList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	
	//SHOW REQUEST PASSES based on selected employee
	$scope.showRequestPasses = function(selectedEmployeeId) {
		$http({
			method : "GET",
			url : "/requests/" + selectedEmployeeId
		}).then(function success(response) {
			$scope.requestsList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	
	//APPROVE Request Pass of selected employee
	$scope.approveRequest = function(requestPassId) {
		$http({
			method : "PUT",
			url : "/requests/approve",
			params : { 
				"requestPassId" : requestPassId,
				"approverId" : currentEmployeeId
			}
		}).then(function success(response) {
			$scope.showRequestPasses();
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};

	//REJECT Request Pass of selected employee
	$scope.rejectRequest = function(requestPassId) {
		$http({
			method : "PUT",
			url : "/requests/reject",
			params : { 
				"requestPassId" : requestPassId,
				"approverId" : currentEmployeeId
			}
		}).then(function success(response) {
			$scope.showRequestPasses();
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	
});