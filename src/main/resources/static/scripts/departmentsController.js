
app.controller('departmentsController', function($scope, $http, $timeout) {

	$timeout(function () {
        if (localStorage.getItem("currentUserRole") == "Administrator") {
    		$scope.isShownFlag = true;
    	} else {
    		$scope.isShownFlag = false;
    	}
    }, 1000);
	
	//INITIAL LOAD
	$scope.initialLoad = function() {
		$scope.departmentId = -1;
		$http({
			method : "GET",
			url : "/departments"
		}).then(function success(response) {
			$scope.departmentsList =  response.data;
		}, function error(response) {
			alert(response.data.error + ": " + response.data.message);
		});

		$http({
			method : "GET",
			url : "/employees/DEPT"
		}).then(function success(response) {
			$scope.deptManagersList = response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
		$http({
			method : "GET",
			url : "/employees/TECH"
		}).then(function success(response) {
			$scope.techManagersList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	$scope.initialLoad();
	
	//SHOW DEPARTMENT information
	$scope.showInfo = function(departmentId) {
		$http({
			method : "GET",
			url : "/departments/" + departmentId
		}).then(function success(response) {
			$scope.departmentId = response.data.id;
			$scope.deptCodeModel =  response.data.deptCode;
			$scope.deptNameModel =  response.data.deptName;
			
			if (response.data.deptManager != null) {
				$scope.deptMgrModel =  response.data.deptManager.id;
			} else {
				$scope.deptMgrModel = -1;
			}
			if (response.data.techManager1 != null) {
				$scope.techMgr1Model =  response.data.techManager1.id;
			} else {
				$scope.techMgr1Model = -1;
			}
			if (response.data.techManager2 != null) {
				$scope.techMgr2Model =  response.data.techManager2.id;
			} else {
				$scope.techMgr2Model = -1;
			}
			if (response.data.techManager3 != null) {
				$scope.techMgr3Model =  response.data.techManager3.id;
			} else {
				$scope.techMgr3Model = -1;
			}
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	
	//SAVE Department Info
	$scope.save = function() {
		var deptDto = {
			"id" : $scope.departmentId,
			"deptCode" : $scope.deptCodeModel,
			"deptName" : $scope.deptNameModel,
			"deptManager" : {
				"id" : $scope.deptMgrModel
			},
			"techManager1" : {
				"id" : $scope.techMgr1Model
			},
			"techManager2" : {
				"id" : $scope.techMgr2Model
			},
			"techManager3" : {
				"id" : $scope.techMgr3Model
			}
		}
		
		//Department Info does not exist yet
		if ($scope.departmentId < 0) {
			
			$http({
				method : "POST",
				url : "/departments",
				data : JSON.stringify(deptDto)
			}).then(function success(response) {
				console.log("SUCCESS");
			}, function error(response) {
				alert(response.data.error + " : " + response.data.message);
			});
		} else {
			$http({
				method : "PUT",
				url : "/departments",
				data : JSON.stringify(deptDto)
			}).then(function success(response) {
				console.log("SUCCESS");
				$scope.initialLoad();
			}, function error(response) {
				alert(response.data.error + " : " + response.data.message);
			});
		}
	};

	//DELETE Department Info
	$scope.delete = function() {
		$http({
			method : "DELETE",
			url : "/departments/" + $scope.departmentId
		}).then(function success(response) {
			console.log("SUCCESS");
			$scope.initialLoad();
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	
});