
app.controller('teamsController', function($scope, $http, $location, $timeout) {

	$timeout(function () {
        if (localStorage.getItem("currentUserRole") != "Team Member") {
    		$scope.isShownFlag = true;
    	} else {
    		$scope.isShownFlag = false;
    	}
    }, 1000);
	
	//RESET Input Fields
	$scope.reset = function() {
		$scope.teamId = -1;
		$scope.deptModel =  -1;
		$scope.projectNameModel =  "";
		$scope.techMgrModel =  -1;
		$scope.teamLeaderModel =  -1;
		$scope.employeeModel =  -1;
		$scope.teamMembersList =  [];
	};
	
	//INITIAL LOAD
	$scope.initialLoad = function() {
		$scope.teamId = -1;
		$http({
			method : "GET",
			url : "/teams"
		}).then(function success(response) {
			$scope.teamsList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
		
		$http({
			method : "GET",
			url : "/departments"
		}).then(function success(response) {
			$scope.departmentsList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
		$http({
			method : "GET",
			url : "/employees/TECH"
		}).then(function success(response) {
			$scope.techManagersList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
		$http({
			method : "GET",
			url : "/employees/TLEAD"
		}).then(function success(response) {
			$scope.teamLeadersList = response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
		$http({
			method : "GET",
			url : "/employees/MEMBER"
		}).then(function success(response) {
			$scope.teamMembersRoleList = response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	$scope.initialLoad();
	
	$scope.orderByThis = function(orderSelected) {
		$scope.orderBySelected = orderSelected;
	};
	
	//SHOW TEAM information
	$scope.showInfo = function(teamId) {
		$http({
			method : "GET",
			url : "/teams/" + teamId
		}).then(function success(response) {
			$scope.reset();
			$scope.teamId = response.data.id;
			$scope.deptModel =  response.data.department.id;
			$scope.projectNameModel =  response.data.projectName;
			
			var teamMembers = [];
			for(var index in response.data.members) {
				var member = response.data.members[index];
				switch(member.departmentRole) {
				case 'Technical Manager' : $scope.techMgrModel = member.id; break;
				case 'Team Leader' : $scope.teamLeaderModel = member.id; break;
				case 'Team Member' : teamMembers.push(member); break;
				}
			}
			$scope.teamMembersList = teamMembers;			
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	
	//ADD New Team Member
	$scope.addMember = function() {
		var teamMembers = $scope.teamMembersList;
		var isMemberNotInList = true;
		for (index in teamMembers) {
			if (teamMembers[index].id == $scope.employeeModel) {
				isMemberNotInList = false;
				break;
			}
		}
		
		if (isMemberNotInList) {
			$http({
				method : "GET",
				url : "/employees/profile/" + $scope.employeeModel
			}).then(function success(response) {
				var member = response.data;
				if ( member.departmentRole == 'Team Member') {
//					teamMembers.push(member);
					$scope.teamMembersList.push(member);
				}
//				$scope.teamMembersList = teamMembers;
			}, function error(response) {
				alert(response.data.error + " : " + response.data.message);
			});
		}
	};
	
	//REMOVE Team Member from list
	$scope.removeMember = function(removeMemberId) {
		var teamMembers = $scope.teamMembersList;
		for (var index in teamMembers) {
			if (teamMembers[index].id == removeMemberId) {
				teamMembers.splice(index, 1);
				break;
			}
		}
		$scope.teamMembersList = teamMembers;
	}
	
	//SAVE Team Structure Info
	$scope.save = function() {
		var teamDto = {
			"id" : $scope.teamId,
			"projectName" : $scope.projectNameModel,
			"department" : {
				'id' : $scope.deptModel
			}
		}
		teamDto.members = $scope.teamMembersList;
		teamDto.members.push({ 
			"id" : $scope.techMgrModel,
			"departmentRole" : "Technical Manager"
		});
		teamDto.members.push({ 
			"id" : $scope.teamLeaderModel,
			"departmentRole" : "Team Leader"
		});
		
		//Team Structure Info does not exist yet
		if ($scope.teamId < 0) {			
			$http({
				method : "POST",
				url : "/teams",
				data : JSON.stringify(teamDto)
			}).then(function success(response) {
				console.log("SUCCESS");
				$scope.initialLoad();
				$scope.showInfo($scope.teamId);
			}, function error(response) {
				alert(response.data.error + " : " + response.data.message);
			});
		} else {
			$http({
				method : "PUT",
				url : "/teams",
				data : JSON.stringify(teamDto)
			}).then(function success(response) {
				console.log("SUCCESS");
				$scope.initialLoad();
				$scope.showInfo($scope.teamId);
			}, function error(response) {
				alert(response.data.error + " : " + response.data.message);
			});
		}
	};

	//DELETE Team Structure Info
	$scope.delete = function() {
		$http({
			method : "DELETE",
			url : "/teams/" + $scope.teamId
		}).then(function success(response) {
			console.log("SUCCESS");
			$scope.initialLoad();
			$scope.reset();
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	
	$scope.viewMemberInfo = function(employeeId) {
		$location.path('/profile/' + employeeId)
	};
	
});