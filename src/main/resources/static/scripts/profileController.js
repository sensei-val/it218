
app.controller('profileController', function($scope, $http, $location, $routeParams, $timeout) {
	
	var currentEmployeeId = localStorage.getItem("currentUserId");
	$timeout(function () {
        if (localStorage.getItem("currentUserRole") == "Administrator") {
    		$scope.isShownFlag = true;
    	} else {
    		$scope.isShownFlag = false;
    	}
    }, 1000);
	
	
	if ($routeParams.employeeId != null) {
		$scope.employeeIdModel = parseInt($routeParams.employeeId, 10);
		$("#deleteBtn").removeAttr("disabled");
	} else {
		$scope.employeeIdModel = currentEmployeeId;
		$("#deleteBtn").attr("disabled", true);
	}
	
	//INITIAL LOAD
	$scope.initialLoad = function() {
				
		$http({
			method : "GET",
			url : "/departments"
		}).then(function success(response) {
			$scope.departmentsList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});	

		$http({
			method : "GET",
			url : "/departments/roles"
		}).then(function success(response) {
			$scope.departmentRolesList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
		
		$http({
			method : "GET",
			url : "/employees/profile/" + $scope.employeeIdModel
		}).then(function success(response) {
			var profile = response.data;
			$scope.employeeIdModel =  profile.id;
			$scope.lastNameIdModel =  profile.lastName;
			$scope.firstNameIdModel =  profile.firstName;
			$scope.addressModel =  profile.address;
			$scope.birthdayModel =  new Date(profile.birthday);
			$scope.currentPasswordModel =  profile.password;
			$scope.departmentIdModel =  profile.department.id;
			$scope.departmentRoleModel = profile.departmentRole;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	$scope.initialLoad();
		
	//SAVE Employee Profile Info
	$scope.save = function() {
		
		
		var employeeDto = {
			"id" : $scope.employeeIdModel,
			"lastName" : $scope.lastNameIdModel,
			"firstName" : $scope.firstNameIdModel,
			"address" : $scope.addressModel,
			"birthday" : $scope.birthdayModel,
			"department" : {
				'id' : $scope.departmentIdModel
			},
			"departmentRole" : $scope.departmentRoleModel
		};

		if ($scope.newPasswordModel && $scope.confirmPasswordModel) {
			if ($scope.newPasswordModel != $scope.confirmPasswordModel) {
				alert("New Password does not match with Confirmation Password.");
			} else {
				employeeDto.password = $scope.newPasswordModel;
			}
		} else {
			employeeDto.password = $scope.currentPasswordModel;
		}
		
		//Team Structure Info does not exist yet
		if (currentEmployeeId < 0) {			
			$http({
				method : "POST",
				url : "/employees",
				data : JSON.stringify(employeeDto)
			}).then(function success(response) {
				console.log("SUCCESS");
				$scope.initialLoad();
			}, function error(response) {
				alert(response.data.error + " : " + response.data.message);
			});
		} else {
			$http({
				method : "PUT",
				url : "/employees",
				data : JSON.stringify(employeeDto)
			}).then(function success(response) {
				console.log("SUCCESS");
				$scope.initialLoad();
			}, function error(response) {
				alert(response.data.error + " : " + response.data.message);
			});
		}
	};

	//DELETE Employee Profile Info
	$scope.delete = function() {
		$http({
			method : "DELETE",
			url : "/employees/" + currentEmployeeId
		}).then(function success(response) {
			console.log("SUCCESS");
			$location.path('/employees');
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	
});