
app.controller('employeesListController', function($scope, $http, $location) {

	//INITIAL LOAD
	$scope.initialLoad = function() {
		$http({
			method : "GET",
			url : "/departments"
		}).then(function success(response) {
			$scope.departmentsList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
		
		$http({
			method : "GET",
			url : "/teams"
		}).then(function success(response) {
			$scope.teamsList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	}
	$scope.initialLoad();
	
	//SEARCH FUNCTION
	$scope.search = function() {
			
		$http({
			method : "GET",
			url : "/employees/search",
			params : { 
				"employeeIdName": $scope.employeeInfoModel,
				"deptId" : $scope.departmentModel,
				"teamId" : $scope.teamModel
			}
		}).then(function success(response) {
			$scope.employeesList =  response.data;
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	};
	
	$scope.viewEmployeeInfo = function(employeeId) {
		$location.path('/profile/' + employeeId)
	};
	
	$scope.orderByThis = function(orderSelected) {
		$scope.orderBySelected = orderSelected;
	};
	
});