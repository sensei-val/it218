var app = angular.module("myApp", ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider
	.when("/", {
		templateUrl : "templates/signin.html",
		controller : "signinController"
	})
	.when("/login", {
		templateUrl : "templates/signin.html",
		controller : "signinController"
	})
	.when("/attendance", {
		templateUrl : "templates/attendance.html",
		controller : "attendanceController"
	})
	.when("/profile", {
		templateUrl : "templates/profile.html",
		controller : "profileController"
	})
	.when("/profile/:employeeId", {
		templateUrl : "templates/profile.html",
		controller : "profileController"
	})
	.when("/logs", {
		templateUrl : "templates/logs.html",
		controller : "logsController"
	})
	.when("/passes", {
		templateUrl : "templates/passes.html",
		controller : "passesController"
	})
	.when("/departments", {
		templateUrl : "templates/departments.html",
		controller : "departmentsController"
	})
	.when("/teams", {
		templateUrl : "templates/teams.html",
		controller : "teamsController"
	})
	.when("/employees", {
		templateUrl : "templates/employeesList.html",
		controller : "employeesListController"
	})
	.when("/logout", {
		templateUrl : "templates/signin.html",
		controller : "signinController"
	})
});

app.run(function($rootScope, $http, $location) {
    // keep user logged in after page refresh
	var currentUserId = localStorage.getItem("currentUserId");
    if (currentUserId) {
        $http.defaults.headers.common.Authorization = '' + localStorage.getItem("token");
    }

    // redirect to login page if not logged in and trying to access a restricted page
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
    	var currentUser = localStorage.getItem("currentUser");
        var publicPages = ['/login'];
        var restrictedPage = publicPages.indexOf($location.path()) === -1;
        if (restrictedPage && !currentUserId) {
            $location.path('/login');
        }
    });
});