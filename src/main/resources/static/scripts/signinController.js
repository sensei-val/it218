
app.controller('signinController', function($scope, $http, $location) {
	
	initController();
	
	function initController() {
        // reset login status
        Logout();
    };
	
	$scope.login = function() {
        $http({
        	method : "POST",
        	url : "/login",
        	data : {
        		"id" : $scope.loginId, 
        		"password" : $scope.loginPassword
        	}
        }).then(function (response) {
            // login successful if there's a token in the response
            if (response.headers('Authorization')) {
                // store token in local storage to keep user logged in between page refreshes
                localStorage.setItem("token", response.headers('Authorization'));
                
                // add jwt token to auth header for all requests made by the $http service
                $http.defaults.headers.common.Authorization = '' + response.headers('Authorization');
                
                // store employee info in local storage
                setEmployeeInfo();
                
                $location.path('/attendance');
                
            } else {
            	console.log("Invalid login");
            }
        }, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
        
        function setEmployeeInfo() {
        	$http({
    			method : "GET",
    			url : "/employees/profile/" + $scope.loginId
    		}).then(function success(response) {
    			var profile = response.data;
            	localStorage.setItem("currentUserId", profile.id);
            	localStorage.setItem("currentUserName", profile.firstName + " " + profile.lastName);
            	localStorage.setItem("currentUserRole", profile.departmentRole);
    		}, function error(response) {
    			alert(response.data.error + " : " + response.data.message);
    		});
        }
    }
	
	function Logout() {
        // remove user from local storage and clear http auth header
        localStorage.removeItem("currentUser");
        $http.defaults.headers.common.Authorization = '';
    }
	
});