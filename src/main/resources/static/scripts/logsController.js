
app.controller('logsController', function($scope, $http) {

	var currentEmployeeId = localStorage.getItem("currentUserId");

	//INITIAL LOAD
	$scope.initialLoad = function() {
		$scope.calendar = populateYearBox();
		
		var currentDate = new Date();
		// Month value of JS Date is 0-based
		var startDate = currentDate.getFullYear() + "-" + 
		("00" + currentDate.getMonth()+1).slice(-2) + "-01";

		refreshLogs(startDate);
	}
	$scope.initialLoad();
	
	$scope.orderByThis = function(orderSelected) {
		$scope.orderBySelected = orderSelected;
	};
	
	function refreshLogs(selectedDate) {
		$scope.calendarModel = selectedDate;

		$http({
			method : "GET",
			url : "/attendance/review",
			params : {
				"reviewerId" : currentEmployeeId,
				"startDate" : selectedDate
			}
		}).then(function success(response) {
			$scope.calendarLogs =  response.data;
			$scope.monthDates = getMonthDates(selectedDate);
		}, function error(response) {
			alert(response.data.error + " : " + response.data.message);
		});
	}
	
	function populateYearBox() {
		var yearBox = [];
		var currentDate = new Date();
		var currentYear = currentDate.getFullYear();
		
		//Generate date with format: yyyy-MM-dd
		for (var startYear = currentYear - 1; startYear <= currentYear + 1; startYear++) {
			for (var month = 1; month <= 12; month++) {
				yearBox.push(startYear + "-" + ("00" + month).slice(-2) + "-01");
			}
		}
		return yearBox;
	}
	
	function getMonthDates(selectedDate) {
		var monthDates = [];
		var subColumns = [];
		var dates = selectedDate.split("-");

		// month column in JS is 0-based
		// 0 is the last date of the previous month
		var lastDay = new Date(dates[0], dates[1], 0).getDate();
		
		for (var day = 1; day <= lastDay; day++) {
			monthDates.push(("00" + dates[1]).slice(-2) + "/" 
					+ ("00" + day).slice(-2));
			subColumns.push("WT");
			subColumns.push("OT");
//			subColumns.push("UT");
		}
		$scope.subColumns = subColumns;
		return monthDates;
	};
	
	$scope.employeeLogs = function populateTable(employeeId) {
		var timeLogs = [];
		$scope.calendarLogs.forEach(function(attendance) {
			if (attendance.employeeInfo.id == employeeId) {
				
				var dates = $scope.calendarModel.split("-");
				var lastDay = new Date(dates[0], dates[1], 0).getDate();
				
				for (var day = 1; day <= lastDay; day++) {
					var date = dates[0] + "-" 
							+ ("00" + dates[1]).slice(-2) + "-" 
							+ ("00" + day).slice(-2);					
					if (attendance.timeLogs[date] != null) {
						timeLogs.push(attendance.timeLogs[date].workHours.toFixed(2));
						timeLogs.push(attendance.timeLogs[date].overtime == 0 ? 
								"" : attendance.timeLogs[date].overtime.toFixed(2));
					} else {
						timeLogs.push("");
						timeLogs.push("");
					}
				}
			}
		});
		return timeLogs;
		
		
	}
});