DROP TABLE IF EXISTS public.team_employee;
DROP TABLE IF EXISTS public.team;
DROP TABLE IF EXISTS public.attendance;
DROP TABLE IF EXISTS public.request_pass;
DROP TABLE IF EXISTS public.employee;
DROP TABLE IF EXISTS public.department;

-------------------------------------DEPARTMENT ENTITY
CREATE TABLE public.department (
 id SERIAL NOT NULL,
 dept_code VARCHAR(10),
 dept_name VARCHAR(50),
 created_date TIMESTAMP,
 updated_date TIMESTAMP
);

ALTER TABLE public.department ADD CONSTRAINT PK_department PRIMARY KEY (id);

-------------------------------------TEAM ENTITY
CREATE TABLE public.team (
 id SERIAL NOT NULL,
 dept_id INT NOT NULL,
 project_name VARCHAR(50),
 created_date TIMESTAMP,
 updated_date TIMESTAMP
);

ALTER TABLE public.team ADD CONSTRAINT PK_team PRIMARY KEY (id,dept_id);
ALTER TABLE public.team ADD CONSTRAINT FK_team_0 FOREIGN KEY (dept_id) REFERENCES department (id);

-------------------------------------EMPLOYEE ENTITY
CREATE TABLE public.employee (
 id SERIAL NOT NULL,
 password VARCHAR(60) NOT NULL,
 first_name VARCHAR(30) NOT NULL,
 last_name VARCHAR(30) NOT NULL,
 address VARCHAR(100),
 birthday DATE,
 department_id INT,
 department_role VARCHAR(20), 
 created_date TIMESTAMP,
 updated_date TIMESTAMP
);

ALTER TABLE public.employee ADD CONSTRAINT PK_employee PRIMARY KEY (id);
ALTER TABLE public.employee ADD CONSTRAINT FK_employee_0 FOREIGN KEY (department_id) REFERENCES department (id);

-------------------------------------TEAM_EMPLOYEE COMPOSITE ENTITY
CREATE TABLE public.team_employee (
 team_id INT NOT NULL,
 member_id INT NOT NULL,
 role VARCHAR(20)
);

ALTER TABLE public.team_employee ADD CONSTRAINT PK_team_employee PRIMARY KEY (team_id,member_id);


-------------------------------------ATTENDANCE ENTITY
CREATE TABLE public.attendance (
 id SERIAL NOT NULL,
 employee_id INT NOT NULL,
 date DATE NOT NULL,
 timeIn TIMESTAMP,
 timeOut TIMESTAMP,
 created_date TIMESTAMP,
 updated_date TIMESTAMP
);

ALTER TABLE public.attendance ADD CONSTRAINT PK_attendance PRIMARY KEY (id);
ALTER TABLE public.attendance ADD CONSTRAINT FK_attendance_0 FOREIGN KEY (employee_id) REFERENCES employee (id);


-------------------------------------REQUEST PASSES ENTITY
CREATE TABLE public.request_pass (
 id SERIAL NOT NULL,
 date TIMESTAMP NOT NULL,
 requestor_id INT NOT NULL,
 approver_id INT,
 pass_status INT,
 created_date TIMESTAMP,
 updated_date TIMESTAMP
);

ALTER TABLE public.request_pass ADD CONSTRAINT PK_request_pass PRIMARY KEY (id);
ALTER TABLE public.request_pass ADD CONSTRAINT FK_request_pass_0 FOREIGN KEY (requestor_id) REFERENCES employee (id);
ALTER TABLE public.request_pass ADD CONSTRAINT FK_request_pass_1 FOREIGN KEY (approver_id) REFERENCES employee (id);


ALTER SEQUENCE public.attendance_id_seq MINVALUE 99 RESTART 100 START 100;
ALTER SEQUENCE public.department_id_seq MINVALUE 99 RESTART 100 START 100;
ALTER SEQUENCE public.employee_id_seq MINVALUE 99 RESTART 100 START 100;
ALTER SEQUENCE public.request_pass_id_seq MINVALUE 99 RESTART 100 START 100;
ALTER SEQUENCE public.team_id_seq MINVALUE 99 RESTART 100 START 100;