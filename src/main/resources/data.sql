INSERT INTO public.department(
            id, dept_code, dept_name, created_date, updated_date)
    VALUES 
    	(0, 'SOFDEV', 'SOFTWARE DEVELOPMENT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    	(1, 'HARDEV', 'HARDWARE DEVELOPMENT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
    

INSERT INTO public.team(
            id, dept_id, project_name, created_date, updated_date)
    VALUES 
    	(0, 0, 'MY TEAM 1', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    	(1, 1, 'MY TEAM 2', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
    
INSERT INTO public.employee(
            id, password, first_name, last_name, address, birthday, department_id, 
            department_role, created_date, updated_date)
    VALUES 
    	(0, 'password', 'ADMIN', 'PEREZ', 'TEJERO', '2/5/1992', 0, 
            'ADMINISTRATOR', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (1, 'password', 'DEPT MANAGER 1', 'PEREZ', 'TEJERO', '2/5/1992', 0, 
            'DEPARTMENT_MANAGER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (2, 'password', 'TECH MANAGER 1', 'PEREZ', 'TEJERO', '2/5/1992', 1, 
            'TECHNICAL_MANAGER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (3, 'password', 'TEAM LEADER 1', 'PEREZ', 'TEJERO', '2/5/1992', 0, 
            'TEAM_LEADER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (4, 'password', 'TEAM MEMBER 1', 'PEREZ', 'TEJERO', '2/5/1992', 0, 
            'TEAM_MEMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (5, 'password', 'TEAM MEMBER 2', 'PEREZ', 'TEJERO', '2/5/1992', 0, 
            'TEAM_MEMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (6, 'password', 'TEAM MEMBER 3', 'PEREZ', 'TEJERO', '2/5/1992', 0, 
            'TEAM_MEMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO public.team_employee(team_id, member_id)
    VALUES (0, 2), (0, 3), (0, 4), (0, 5),
    	(1, 1), (0, 6);

INSERT INTO public.attendance(id, employee_id, date, timein, timeout)
	VALUES
		(0, 0, '1/13/2018', '1/13/2018 08:00:00', '1/13/2018 17:00:00'),
		(1, 0, '1/14/2018', '1/14/2018 08:00:00', '1/14/2018 17:30:00'),
		(2, 0, '1/15/2018', '1/15/2018 08:00:00', '1/15/2018 20:45:00'),
		(3, 0, '1/16/2018', '1/16/2018 08:00:00', '1/17/2018 01:45:00'),
		(4, 0, '1/17/2018', '1/17/2018 08:00:00', '1/17/2018 15:00:00'),
		(5, 0, '1/18/2018', '1/18/2018 08:00:00', '1/18/2018 16:30:00'),
		(6, 1, '1/01/2018', '1/01/2018 08:00:00', '1/01/2018 17:30:00'),
		(7, 1, '1/02/2018', '1/01/2018 08:00:00', '1/01/2018 17:30:00'),
		(8, 2, '1/01/2018', '1/01/2018 08:00:00', '1/01/2018 17:30:00'),
		(9, 2, '1/02/2018', '1/02/2018 08:00:00', '1/02/2018 17:30:00'),
		(10, 3, '1/01/2018', '1/01/2018 08:00:00', '1/01/2018 17:30:00'),
		(11, 4, '1/01/2018', '1/01/2018 08:00:00', '1/01/2018 17:30:00'),
		(12, 5, '1/01/2018', '1/01/2018 08:00:00', '1/01/2018 17:30:00'),
		(13, 5, '1/02/2018', '1/02/2018 08:00:00', '1/02/2018 17:30:00');

/*
INSERT INTO public.request_pass(
            id, date, requestor_id, approver_id, pass_status, created_date, 
            updated_date)
    VALUES (0, '2018-01-10 18:12:41.66538', 0, null, 0, CURRENT_TIMESTAMP, 
            CURRENT_TIMESTAMP);
*/