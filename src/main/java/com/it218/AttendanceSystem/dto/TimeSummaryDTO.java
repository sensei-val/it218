/**
 * 
 */
package com.it218.AttendanceSystem.dto;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 * @author reneir.val.t.perez
 *
 */
public class TimeSummaryDTO {

    private EmployeeDTO employeeInfo;
    private Map<LocalDate, TimeLogsDTO>	timeLogs;
    
    public TimeSummaryDTO() {
    	timeLogs = new HashMap<>();
    }

	public EmployeeDTO getEmployeeInfo() {
		return employeeInfo;
	}

	public void setEmployeeInfo(EmployeeDTO employeeInfo) {
		this.employeeInfo = employeeInfo;
	}

	public Map<LocalDate, TimeLogsDTO> getTimeLogs() {
		return timeLogs;
	}

	public void setTimeLogs(Map<LocalDate, TimeLogsDTO> timeLogsLogs) {
		this.timeLogs = timeLogsLogs;
	}
	
	public void addTimeLog(LocalDate date, TimeLogsDTO timeLog) {
		this.timeLogs.put(date, timeLog);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((employeeInfo.getId() == null) ? 0 : employeeInfo.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeSummaryDTO other = (TimeSummaryDTO) obj;
		if (employeeInfo == null) {
			if (other.employeeInfo != null)
				return false;
		} else if (!employeeInfo.getId().equals(other.employeeInfo.getId()))
			return false;
		return true;
	}
	
	
}
