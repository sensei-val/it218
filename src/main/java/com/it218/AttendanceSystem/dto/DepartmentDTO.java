/**
 * 
 */
package com.it218.AttendanceSystem.dto;

import javax.validation.constraints.Size;

/**
 * @author reneir.val.t.perez
 *
 */
public class DepartmentDTO {

    private Integer   id;
    
    @Size(max=10)
    private String    deptCode;
    
    @Size(max=50)
    private String    deptName;
    
    private EmployeeDTO deptManager;
    private EmployeeDTO techManager1;
    private EmployeeDTO techManager2;
    private EmployeeDTO techManager3;
    
    public DepartmentDTO() {
    	
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public EmployeeDTO getDeptManager() {
		return deptManager;
	}

	public void setDeptManager(EmployeeDTO deptManager) {
		this.deptManager = deptManager;
	}

	public EmployeeDTO getTechManager1() {
		return techManager1;
	}

	public void setTechManager1(EmployeeDTO techManager1) {
		this.techManager1 = techManager1;
	}

	public EmployeeDTO getTechManager2() {
		return techManager2;
	}

	public void setTechManager2(EmployeeDTO techManager2) {
		this.techManager2 = techManager2;
	}

	public EmployeeDTO getTechManager3() {
		return techManager3;
	}

	public void setTechManager3(EmployeeDTO techManager3) {
		this.techManager3 = techManager3;
	}
    
    
}
