/**
 * 
 */
package com.it218.AttendanceSystem.dto;

/**
 * @author rvalperez
 *
 */
public class TimeLogsDTO {
	
	private Float workHours;
	private Float overtime;
	private Float undertime;
	
	public TimeLogsDTO() {}
	
	public TimeLogsDTO(Float workHours, Float overtime, Float undertime) {
		super();
		this.workHours = workHours;
		this.overtime = overtime;
		this.undertime = undertime;
	}

	public Float getWorkHours() {
		return workHours;
	}
	public void setWorkHours(Float workHours) {
		this.workHours = workHours;
	}
	public Float getOvertime() {
		return overtime;
	}
	public void setOvertime(Float overtime) {
		this.overtime = overtime;
	}
	public Float getUndertime() {
		return undertime;
	}
	public void setUndertime(Float undertime) {
		this.undertime = undertime;
	}
}
