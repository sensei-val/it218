/**
 * 
 */
package com.it218.AttendanceSystem.dto;

import java.sql.Timestamp;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.it218.AttendanceSystem.util.enums.RequestPassStatus;

/**
 * @author reneir.val.t.perez
 *
 */
public class RequestPassDTO {

	private Integer id;
	
	@Temporal(TemporalType.DATE)
	private Timestamp date;
	
	private Integer requestorId;
	private String requestorName;
	private Integer approverId;
	private String approverName;
	private RequestPassStatus passStatus;

	public RequestPassDTO() {

	}

	public RequestPassDTO(Integer id, Timestamp date, Integer requestorId, String requestorName, Integer approverId,
			String approverName, RequestPassStatus passStatus) {
		super();
		this.id = id;
		this.date = date;
		this.requestorId = requestorId;
		this.requestorName = requestorName;
		this.approverId = approverId;
		this.approverName = approverName;
		this.passStatus = passStatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		
		this.date = date;
	}

	public Integer getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(Integer requestorId) {
		this.requestorId = requestorId;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public Integer getApproverId() {
		return approverId;
	}

	public void setApproverId(Integer approverId) {
		this.approverId = approverId;
	}

	public String getApproverName() {
		return approverName;
	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	public RequestPassStatus getPassStatus() {
		return passStatus;
	}

	public void setPassStatus(RequestPassStatus passStatus) {
		this.passStatus = passStatus;
	}

	
}
