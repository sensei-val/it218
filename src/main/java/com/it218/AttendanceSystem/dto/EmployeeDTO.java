/**
 * 
 */
package com.it218.AttendanceSystem.dto;

import java.util.Date;

import javax.validation.constraints.Size;

import com.it218.AttendanceSystem.util.enums.DepartmentRole;

/**
 * @author reneir.val.t.perez
 *
 */
public class EmployeeDTO {

	private Integer   	id;
	
	private String		password;
	
	@Size(max=30)
    private String    	firstName;
	
	@Size(max=30)
    private String    	lastName;
	
	@Size(max=100)
    private String    	address;
	
    private Date 	  	birthday;
    
    private DepartmentDTO   department;
    
    private DepartmentRole 	departmentRole;
    
    public EmployeeDTO() {
    	
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public DepartmentDTO getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentDTO department) {
		this.department = department;
	}

	public DepartmentRole getDepartmentRole() {
		return departmentRole;
	}

	public void setDepartmentRole(DepartmentRole departmentRole) {
		this.departmentRole = departmentRole;
	}
	
    
}
