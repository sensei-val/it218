/**
 * 
 */
package com.it218.AttendanceSystem.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author reneir.val.t.perez
 *
 */
public class AttendanceDTO {

    private Integer   id;
    private EmployeeDTO   employee;
    
    @JsonFormat(pattern = "MMM dd, yyyy")
    private LocalDate	date;
    
    @JsonFormat(pattern = "HH:mm:ss")
    private LocalDateTime   timein;
    
    @JsonFormat(pattern = "HH:mm:ss")
    private LocalDateTime   timeout;
    private Float		total;
    private String		remarks;
    
    public AttendanceDTO() {
    	
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EmployeeDTO getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeDTO employee) {
		this.employee = employee;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalDateTime getTimein() {
		return timein;
	}

	public void setTimein(LocalDateTime timein) {
		this.timein = timein;
	}

	public LocalDateTime getTimeout() {
		return timeout;
	}

	public void setTimeout(LocalDateTime timeout) {
		this.timeout = timeout;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
    	
}
