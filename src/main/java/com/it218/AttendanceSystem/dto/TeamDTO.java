/**
 * 
 */
package com.it218.AttendanceSystem.dto;

import java.util.List;

import javax.validation.constraints.Size;

/**
 * @author reneir.val.t.perez
 *
 */
public class TeamDTO {

	private Integer   id;
	
	@Size(max=50)
    private String    projectName;
	
    private DepartmentDTO   department;
    
    private List<EmployeeDTO> members;
    
    public TeamDTO() {
    	
    }
    
	public TeamDTO(Integer id, String projectName, DepartmentDTO department, List<EmployeeDTO> members) {
		super();
		this.id = id;
		this.projectName = projectName;
		this.department = department;
		this.members = members;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public DepartmentDTO getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentDTO department) {
		this.department = department;
	}

	public List<EmployeeDTO> getMembers() {
		return members;
	}

	public void setMembers(List<EmployeeDTO> members) {
		this.members = members;
	}

}
