/**
 * 
 */
package com.it218.AttendanceSystem.dto;

/**
 * @author reneir.val.t.perez
 *
 */
public class SearchResultsDTO {
	
	private EmployeeDTO		employeeInfo;
    private DepartmentDTO   department;
    private TeamDTO 		team;
    
    public SearchResultsDTO() {
    	
    }

	public EmployeeDTO getEmployeeInfo() {
		return employeeInfo;
	}

	public void setEmployeeInfo(EmployeeDTO employeeInfo) {
		this.employeeInfo = employeeInfo;
	}

	public DepartmentDTO getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentDTO department) {
		this.department = department;
	}

	public TeamDTO getTeam() {
		return team;
	}

	public void setTeam(TeamDTO team) {
		this.team = team;
	}
    
    
}
