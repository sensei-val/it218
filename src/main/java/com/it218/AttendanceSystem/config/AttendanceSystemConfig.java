	/**
 * 
 */
package com.it218.AttendanceSystem.config;


import javax.sql.DataSource;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

/**
 * @author reneir.val.t.perez
 *
 */
@Configuration
public class AttendanceSystemConfig {

	@Autowired
	private DataSource dataSource;
	
	@Bean
	public DSLContext create() {
		return new DefaultDSLContext(configuration());
	}
	
	@Bean
	public DataSourceConnectionProvider connectionProvider() {
	    return new DataSourceConnectionProvider(new TransactionAwareDataSourceProxy(dataSource));
	}
	
	public DefaultConfiguration configuration() {
	    DefaultConfiguration jooqConfiguration = new DefaultConfiguration();
	    jooqConfiguration.set(connectionProvider());
	    jooqConfiguration.setSQLDialect(SQLDialect.POSTGRES);
	 
	    return jooqConfiguration;
	}
	
//	@Bean
//	public PasswordEncoder passwordEncoder() {
//	    return new BCryptPasswordEncoder();
//	}
	
}
