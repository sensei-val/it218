/**
 * 
 */
package com.it218.AttendanceSystem.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.jooq.Record;
import org.jooq.Record8;
import org.jooq.maven.postgres.tables.Department;
import org.jooq.maven.postgres.tables.Employee;
import org.jooq.maven.postgres.tables.Team;
import org.jooq.maven.postgres.tables.records.EmployeeRecord;
import org.jooq.maven.postgres.tables.records.TeamEmployeeRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.it218.AttendanceSystem.dao.DepartmentDAO;
import com.it218.AttendanceSystem.dao.EmployeeDAO;
import com.it218.AttendanceSystem.dao.TeamEmployeeDAO;
import com.it218.AttendanceSystem.dto.DepartmentDTO;
import com.it218.AttendanceSystem.dto.EmployeeDTO;
import com.it218.AttendanceSystem.dto.SearchResultsDTO;
import com.it218.AttendanceSystem.dto.TeamDTO;
import com.it218.AttendanceSystem.util.enums.DepartmentRole;
import com.it218.AttendanceSystem.util.mapper.ObjectMapper;

/**
 * @author reneir.val.t.perez
 *
 */
@Service
@Transactional
public class EmployeeMgtService {

	@Autowired
	private EmployeeDAO employeeDao;

	@Autowired
	private DepartmentDAO deptDao;

	@Autowired
	private TeamEmployeeDAO teamEmployeeDao;
	
		
	public List<EmployeeDTO> getAll() {
		List<EmployeeDTO> result = new ArrayList<>(); 
		for (EmployeeRecord employeeRecord : employeeDao.findAll()) {			
			EmployeeDTO employeeDto = ObjectMapper.toEmployeeDTO(
					employeeRecord, 
					deptDao.findOne(employeeRecord.getDepartmentId()), 
					DepartmentRole.valueOf(employeeRecord.getDepartmentRole()));
			result.add(employeeDto);
		}
		return result;
	}
	
	public List<EmployeeDTO> getEmployeesByRole(DepartmentRole role) {
		List<EmployeeDTO> result = new ArrayList<>(); 
		for (EmployeeRecord employeeRecord : employeeDao.findAllEmployeesByRole(role)) {			
			EmployeeDTO employeeDto = ObjectMapper.toEmployeeDTO(
					employeeRecord, 
					deptDao.findOne(employeeRecord.getDepartmentId()), 
					DepartmentRole.valueOf(employeeRecord.getDepartmentRole()));
			result.add(employeeDto);
		}
		return result;
	}
	
	public List<SearchResultsDTO> search(String employeeIdName, Integer deptId, Integer teamId) {
		List<SearchResultsDTO> results = new ArrayList<>();
		List<Record8<Integer, String, String, Integer, String, Integer, String, String>> queryResults = 
				employeeDao.search(employeeIdName, deptId, teamId); 
		for (Record record : queryResults) {
			SearchResultsDTO searchResultItem =  new SearchResultsDTO();
			
			EmployeeDTO employeeInfo = new EmployeeDTO();
			employeeInfo.setId(record.get(Employee.EMPLOYEE.ID));
			employeeInfo.setFirstName(record.get(Employee.EMPLOYEE.FIRST_NAME));
			employeeInfo.setLastName(record.get(Employee.EMPLOYEE.LAST_NAME));
			employeeInfo.setDepartmentRole(DepartmentRole.valueOf(record.get(Employee.EMPLOYEE.DEPARTMENT_ROLE)));
			searchResultItem.setEmployeeInfo(employeeInfo);
			
			DepartmentDTO departmentInfo = new DepartmentDTO();
			departmentInfo.setId(record.get(Department.DEPARTMENT.ID));
			departmentInfo.setDeptName(record.get(Department.DEPARTMENT.DEPT_NAME));
			searchResultItem.setDepartment(departmentInfo);
			
			TeamDTO teamInfo = new TeamDTO();
			teamInfo.setId(record.get(Team.TEAM.ID));
			teamInfo.setProjectName(record.get(Team.TEAM.PROJECT_NAME));
			searchResultItem.setTeam(teamInfo);
			
			results.add(searchResultItem);			
		}
		return results;
	}
		
	public EmployeeDTO getById(Integer id) {
		EmployeeDTO result = null;
		EmployeeRecord employeeRecord = employeeDao.findOne(id);
		
		if (null != employeeRecord) {			
			result = ObjectMapper.toEmployeeDTO(
					employeeRecord, 
					deptDao.findOne(employeeRecord.getDepartmentId()), 
					DepartmentRole.valueOf(employeeRecord.getDepartmentRole()));
		}		
		return result;
	}	

	public EmployeeDTO assignEmployeeDeptRole(Integer employeeId, Integer departmentId, DepartmentRole role) {
		EmployeeRecord employeeRecord = employeeDao.findOne(employeeId);
		employeeRecord.setDepartmentId(departmentId);
		employeeRecord.setDepartmentRole(role.getValue());
		
		EmployeeDTO result = ObjectMapper.toEmployeeDTO(
					employeeDao.updateRecord(employeeRecord), 
					deptDao.findOne(employeeRecord.getDepartmentId()), 
					DepartmentRole.valueOf(employeeRecord.getDepartmentRole()));
		return result;
	}
	
	public EmployeeDTO assignEmployeeTeamRole(Integer employeeId, Integer teamId, DepartmentRole role) {
		TeamEmployeeRecord record = teamEmployeeDao.findByTeamIdAndMemberId(teamId, employeeId);
		if (null == record) {
			record = teamEmployeeDao.addRecord(teamId, employeeId, role.name());
		} else {
			record.setTeamId(teamId);
			record.setMemberId(employeeId);
			record.setRole(role.name());
			record = teamEmployeeDao.updateRecord(record);
		}
		
		EmployeeRecord employeeRecord = employeeDao.findOne(record.getMemberId());
		
		EmployeeDTO result = ObjectMapper.toEmployeeDTO(
					employeeDao.updateRecord(employeeRecord), 
					deptDao.findOne(employeeRecord.getDepartmentId()), 
					DepartmentRole.valueOf(employeeRecord.getDepartmentRole()));
		return result;
	}
	
	public EmployeeDTO addEmployee(EmployeeDTO employee) {
		//Add new employee
		EmployeeRecord employeeRecord = employeeDao.addRecord(ObjectMapper.toEmployeeRecord(employee));
		EmployeeDTO result = ObjectMapper.toEmployeeDTO(
								employeeRecord, 
								deptDao.findOne(employeeRecord.getDepartmentId()), 
								DepartmentRole.valueOf(employeeRecord.getDepartmentRole()));
		return result;
	}
	
	public EmployeeDTO updateEmployee(EmployeeDTO employee) {
		EmployeeRecord employeeRecord = employeeDao.updateRecord(ObjectMapper.toEmployeeRecord(employee));
		EmployeeDTO result = ObjectMapper.toEmployeeDTO(
								employeeRecord, 
								deptDao.findOne(employeeRecord.getDepartmentId()), 
								DepartmentRole.valueOf(employeeRecord.getDepartmentRole()));
		return result;
	}
	
	public void deleteById(Integer employeeId) {
		
		teamEmployeeDao.deleteByMemberId(employeeId);
		
		employeeDao.deleteById(employeeId);
	}
	
	
}
