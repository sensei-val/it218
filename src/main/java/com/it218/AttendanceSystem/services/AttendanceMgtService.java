/**
 * 
 */
package com.it218.AttendanceSystem.services;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.transaction.Transactional;

import org.jooq.Record;
import org.jooq.maven.postgres.tables.records.AttendanceRecord;
import org.jooq.maven.postgres.tables.records.DepartmentRecord;
import org.jooq.maven.postgres.tables.records.EmployeeRecord;
import org.jooq.maven.postgres.tables.records.RequestPassRecord;
import org.jooq.maven.postgres.tables.records.TeamEmployeeRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.it218.AttendanceSystem.dao.AttendanceDAO;
import com.it218.AttendanceSystem.dao.DepartmentDAO;
import com.it218.AttendanceSystem.dao.EmployeeDAO;
import com.it218.AttendanceSystem.dao.RequestPassDAO;
import com.it218.AttendanceSystem.dao.TeamEmployeeDAO;
import com.it218.AttendanceSystem.dto.AttendanceDTO;
import com.it218.AttendanceSystem.dto.EmployeeDTO;
import com.it218.AttendanceSystem.dto.TimeLogsDTO;
import com.it218.AttendanceSystem.dto.TimeSummaryDTO;
import com.it218.AttendanceSystem.util.enums.AttendanceStatus;
import com.it218.AttendanceSystem.util.enums.DepartmentRole;
import com.it218.AttendanceSystem.util.enums.RequestPassStatus;
import com.it218.AttendanceSystem.util.mapper.ObjectMapper;

/**
 * @author reneir.val.t.perez
 *
 */
@Service
@Transactional
public class AttendanceMgtService {

	@Autowired
	private AttendanceDAO attendanceDao;
	
	@Autowired
	private EmployeeDAO employeeDao;

	@Autowired
	private TeamEmployeeDAO teamEmployeeDao;

	@Autowired
	private DepartmentDAO deptDao;
	
	@Autowired
	private RequestPassDAO requestPassDao;
	
	private Float standardWorkHours = 8.0f;
	private Float breakTime = 1.0f;
	
	public List<AttendanceDTO> getAll() {
		List<AttendanceDTO> result = new ArrayList<>(); 
		for (AttendanceRecord record : attendanceDao.findAll()) {
			EmployeeRecord employeeRecord = employeeDao.findOne(record.getEmployeeId());
			DepartmentRecord deptRecord = deptDao.findOne(employeeRecord.getDepartmentId());
			
			AttendanceDTO resultItem = (ObjectMapper.toAttendanceDTO(record, employeeRecord, deptRecord, 
					DepartmentRole.valueOf(employeeRecord.getDepartmentRole())));
			resultItem = this.calculateHours(resultItem);
			result.add(resultItem);
			
		}
		return result;
	}
	
	public List<AttendanceDTO> getAllByEmployeeId(Integer employeeId) {
		List<AttendanceDTO> result = new ArrayList<>(); 
		for (AttendanceRecord record : attendanceDao.findAllByEmployeeId(employeeId)) {
			EmployeeRecord employeeRecord = employeeDao.findOne(record.getEmployeeId());
			DepartmentRecord deptRecord = deptDao.findOne(employeeRecord.getDepartmentId());
			
			result.add(ObjectMapper.toAttendanceDTO(record, employeeRecord, deptRecord, 
					DepartmentRole.valueOf(employeeRecord.getDepartmentRole())));
			AttendanceDTO resultItem = (ObjectMapper.toAttendanceDTO(record, employeeRecord, deptRecord, 
					DepartmentRole.valueOf(employeeRecord.getDepartmentRole())));
			resultItem = this.calculateHours(resultItem);
			result.add(resultItem);
		}
		return result;
	}
	
	public AttendanceDTO getById(Integer id) {
		AttendanceDTO result = null;
		AttendanceRecord record = attendanceDao.findOne(id);
		
		if (null != record) {
			EmployeeRecord employeeRecord = employeeDao.findOne(record.getEmployeeId());
			DepartmentRecord deptRecord = deptDao.findOne(employeeRecord.getDepartmentId());
			
			result = ObjectMapper.toAttendanceDTO(record, employeeRecord, deptRecord, 
					DepartmentRole.valueOf(employeeRecord.getDepartmentRole()));
			result = this.calculateHours(result);
		}		
		return result;
	}
	
	public AttendanceDTO inputTimeIn(AttendanceDTO attendance) throws Exception {
		
		//Check existing attendance records for the day
		List<AttendanceRecord> attendanceRecords = 
				attendanceDao.findAllByEmployeeIdAndDate(attendance.getEmployee().getId(), attendance.getDate());
		
		//Check if there is request pass
		if (!attendanceRecords.isEmpty()) {
			
			List<RequestPassRecord> requestPasses = 
					requestPassDao.findAllRequestsByRequestorIdAndDateAndStatus(
							attendance.getEmployee().getId(), 
							attendance.getDate(), 
							RequestPassStatus.APPROVED);

			//Set Request Pass Status to USED
			if (!requestPasses.isEmpty()) {
				RequestPassRecord toUpdate = requestPasses.get(0);
				toUpdate.setPassStatus(RequestPassStatus.USED.getValue());
				requestPassDao.update(toUpdate);
			} else {
				//TODO: Throw exception
				throw new Exception("Cannot Time In again. No approved Request Pass.");
			}			
		}
				
		AttendanceRecord timeIn = new AttendanceRecord(
				null, 
				attendance.getEmployee().getId(), 
				new Timestamp(System.currentTimeMillis()), 
				new Timestamp(System.currentTimeMillis()), 
				null, 
				null, 
				null);
		
		AttendanceRecord record = attendanceDao.addRecord(timeIn);
		EmployeeRecord employeeRecord = employeeDao.findOne(record.getEmployeeId());
		DepartmentRecord deptRecord = deptDao.findOne(employeeRecord.getDepartmentId());
		
		return ObjectMapper.toAttendanceDTO(record, employeeRecord, deptRecord, 
				DepartmentRole.valueOf(employeeRecord.getDepartmentRole()));
	}
	
	
	public AttendanceDTO inputTimeOut(AttendanceDTO attendance) {
				
		AttendanceRecord timeOut = new AttendanceRecord(
				attendance.getId(), 
				attendance.getEmployee().getId(), 
				new Timestamp(System.currentTimeMillis()), 
				null, 
				new Timestamp(System.currentTimeMillis()), 
				null, 
				null);
		
		AttendanceRecord record = attendanceDao.updateTimeOut(timeOut);

		EmployeeRecord employeeRecord = employeeDao.findOne(record.getEmployeeId());
		DepartmentRecord deptRecord = deptDao.findOne(employeeRecord.getDepartmentId());
		
		return ObjectMapper.toAttendanceDTO(record, employeeRecord, deptRecord, 
				DepartmentRole.valueOf(employeeRecord.getDepartmentRole()));
	}
	
	public void deleteById(Integer id) {
		attendanceDao.deleteById(id);
	}
	
	public Integer getLatestAttendanceRecordId(Integer employeeId, LocalDate date) {
		Integer result = null;
		
		//Check if there is already a registered time in for the selected date
		List<AttendanceRecord> records = 
				attendanceDao.findDAllByDateWithNoTimeOut(employeeId, date);
		if (!records.isEmpty()) {
			result = records.get(0).getId();
		}
		
		return result;
	}
	
	private AttendanceDTO calculateHours(AttendanceDTO attendance) {
		
		Duration diff = Duration.between(attendance.getTimein(), attendance.getTimeout());
		float workHours = (float)diff.toHours() + ((float)diff.toHours() - ((float) diff.toMinutes()/ 60.0f));
		
		//Total Work Hours Minus 1 Hour Break
		attendance.setTotal(workHours - 1);
		
		if (attendance.getTotal() < 8.0f) {
			attendance.setRemarks(AttendanceStatus.UNDER_TIME.getValue());
		} else if (attendance.getTotal() > 8.0f) {
			attendance.setRemarks(AttendanceStatus.OVER_TIME.getValue());
		} else if (attendance.getTotal() == 8.0f) {
			attendance.setRemarks(AttendanceStatus.REGULAR.getValue());
		}
		
		return attendance;		
	}
	
	public Set<TimeSummaryDTO> getSummaryLogs(Integer reviewerId, LocalDate startDate) {
		Set<TimeSummaryDTO> result = new HashSet<>();
		List<Integer> subordinatesList = this.getSubordinateIds(reviewerId);
		
		Map<Integer, Map<LocalDate, TimeLogsDTO>> calendarLogs = new LinkedHashMap<>();
		Map<LocalDate, TimeLogsDTO> timeLogs = new LinkedHashMap<>();
		for (Record record : attendanceDao.summary(subordinatesList, startDate)) {
						
			Integer employeeId = record.get("EMPLOYEE_ID", Integer.class); 
			if (calendarLogs.containsKey(employeeId)) {
				timeLogs = calendarLogs.get(employeeId);
			} else {
				timeLogs = new LinkedHashMap<>();
			}
			
			// Get Time Logs
			TimeLogsDTO timeLog = new TimeLogsDTO();
			BigDecimal workHoursBD = record.get("WORK_HOURS", BigDecimal.class);
			if (workHoursBD != null) {
				Float workHours = (float) (workHoursBD.floatValue() / 1000.0 / 60.0 / 60.0);
				
				if (workHours - this.breakTime >= this.standardWorkHours) {
					timeLog.setWorkHours(this.standardWorkHours);
					timeLog.setOvertime(workHours - this.standardWorkHours - this.breakTime);
					timeLog.setUndertime(0.0f);
				} else {
					if (workHours > this.breakTime) {
						timeLog.setWorkHours(workHours - this.breakTime);
					} else {
						timeLog.setWorkHours(workHours);
					}				
					timeLog.setOvertime(0.0f);
					timeLog.setUndertime(this.standardWorkHours - workHours);
				}
				Timestamp logDate = record.get("DATE", Timestamp.class);
				timeLogs.put(logDate.toLocalDateTime().toLocalDate(), timeLog);
	
				calendarLogs.put(employeeId, timeLogs);
			}
		}
		
		for (Entry<Integer, Map<LocalDate, TimeLogsDTO>> entry : calendarLogs.entrySet()) {

			// Get Employee Details
			EmployeeRecord employeeRecord = employeeDao.findOne(entry.getKey());
			DepartmentRecord departmentRecord = deptDao.findOne(employeeRecord.getDepartmentId());
			EmployeeDTO employeeDto = ObjectMapper.toEmployeeDTO(
					employeeRecord, departmentRecord, DepartmentRole.valueOf(employeeRecord.getDepartmentRole()));
			
			// Add to Set
			TimeSummaryDTO summary = new TimeSummaryDTO();
			summary.setEmployeeInfo(employeeDto);
			summary.setTimeLogs(entry.getValue());
			
			result.add(summary);
		}
		
		return result;
	}
	
	private List<Integer> getSubordinateIds(Integer reviewerId) {
		List<Integer> result = new ArrayList<>();
		
		EmployeeRecord reviewer = employeeDao.findOne(reviewerId);
		switch(DepartmentRole.valueOf(reviewer.getDepartmentRole())) {
		case ADMINISTRATOR:
			for (EmployeeRecord record : employeeDao.findAll()) {
				result.add(record.getId());
			}
			break;
		case DEPARTMENT_MANAGER:
			for (EmployeeRecord record : employeeDao.findAllByDepartmentId(reviewer.getDepartmentId())) {
				result.add(record.getId());
			}
			break;
		case TECHNICAL_MANAGER:
		case TEAM_LEADER:
			for (TeamEmployeeRecord teamRecord : teamEmployeeDao.findByMemberId(reviewerId)) {
				for (TeamEmployeeRecord membersRecord : teamEmployeeDao.findByTeamId(teamRecord.getTeamId())) {
					result.add(membersRecord.getMemberId());
				}
			}
			break;
		case TEAM_MEMBER:
			result.add(reviewerId);
			break;		
		}
		
		return result;
	}
}
