/**
 * 
 */
package com.it218.AttendanceSystem.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.jooq.maven.postgres.tables.records.DepartmentRecord;
import org.jooq.maven.postgres.tables.records.EmployeeRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.it218.AttendanceSystem.dao.DepartmentDAO;
import com.it218.AttendanceSystem.dao.EmployeeDAO;
import com.it218.AttendanceSystem.dto.DepartmentDTO;
import com.it218.AttendanceSystem.util.enums.DepartmentRole;
import com.it218.AttendanceSystem.util.mapper.ObjectMapper;

/**
 * @author reneir.val.t.perez
 *
 */
@Service
@Transactional
public class DepartmentMgtService {

	@Autowired
	private DepartmentDAO deptDao;
	
	@Autowired
	private EmployeeDAO employeeDao;
	
	public List<DepartmentDTO> getAll() {
		List<DepartmentDTO> result = new ArrayList<>(); 
		for (DepartmentRecord deptInfoRecord : deptDao.findAll()) {
			List<EmployeeRecord> deptManagers = employeeDao.findByDeptIdAndRole(deptInfoRecord.getId(), DepartmentRole.DEPARTMENT_MANAGER);
			List<EmployeeRecord> techManagers = employeeDao.findByDeptIdAndRole(deptInfoRecord.getId(), DepartmentRole.TECHNICAL_MANAGER);
			
			DepartmentDTO deptDto = ObjectMapper.toDepartmentDTO(deptInfoRecord, 
					(!deptManagers.isEmpty() ? deptManagers.get(0) : null), 
					(techManagers.size() > 0 ? Optional.of(techManagers.get(0)) : Optional.empty()),
					(techManagers.size() > 1 ? Optional.of(techManagers.get(1)) : Optional.empty()),
					(techManagers.size() > 2 ? Optional.of(techManagers.get(2)) : Optional.empty()));
			
			result.add(deptDto);
		}
		return result;
	}
	
	public DepartmentDTO getById(Integer id) {
		DepartmentDTO result = null;
		DepartmentRecord deptInfoRecord = deptDao.findOne(id);
		
		if (null != deptInfoRecord) {			
			List<EmployeeRecord> deptManagers = employeeDao.findByDeptIdAndRole(deptInfoRecord.getId(), DepartmentRole.DEPARTMENT_MANAGER);
			List<EmployeeRecord> techManagers = employeeDao.findByDeptIdAndRole(deptInfoRecord.getId(), DepartmentRole.TECHNICAL_MANAGER);
			
			result = ObjectMapper.toDepartmentDTO(deptInfoRecord, 
					(!deptManagers.isEmpty() ? deptManagers.get(0) : null), 
					(techManagers.size() > 0 ? Optional.of(techManagers.get(0)) : Optional.empty()),
					(techManagers.size() > 1 ? Optional.of(techManagers.get(1)) : Optional.empty()),
					(techManagers.size() > 2 ? Optional.of(techManagers.get(2)) : Optional.empty()));
		}		
		return result;
	}
	
	public List<DepartmentRole> getAllDepartmentRoles() {
		return Arrays.asList(DepartmentRole.values());
	}
	
	public void addDepartment(DepartmentDTO department) {
		this.updateManagers(department);
		deptDao.addRecord(ObjectMapper.toDepartmentRecord(department));
	}
	
	public void updateDepartment(DepartmentDTO department) {		
		this.updateManagers(department);
		deptDao.update(ObjectMapper.toDepartmentRecord(department));
	}
	
	public void deleteById(Integer departmentId) {
		List<EmployeeRecord> employees = employeeDao.findAllByDepartmentId(departmentId);
		employees.forEach(employee -> {
			EmployeeRecord employeeRecord = employeeDao.findOne(employee.getId());
			employeeRecord.setDepartmentId(null);
			employeeRecord.setDepartmentRole(null);
			employeeDao.updateRecord(employee);
		});
		deptDao.deleteById(departmentId);
	}
	
	private void updateManagers(DepartmentDTO department) {

		if (department.getDeptManager().getId() > -1) {
			EmployeeRecord employeeRecord = employeeDao.findOne(department.getDeptManager().getId());
			employeeRecord.setDepartmentId(department.getId());
			employeeRecord.setDepartmentRole(DepartmentRole.DEPARTMENT_MANAGER.name());
			employeeDao.updateRecord(employeeRecord);
		}
		if (department.getTechManager1().getId() > -1) {
			EmployeeRecord employeeRecord = employeeDao.findOne(department.getTechManager1().getId());
			employeeRecord.setDepartmentId(department.getId());
			employeeRecord.setDepartmentRole(DepartmentRole.TECHNICAL_MANAGER.name());
			employeeDao.updateRecord(employeeRecord);
		}
		if (department.getTechManager2().getId() > -1) {
			EmployeeRecord employeeRecord = employeeDao.findOne(department.getTechManager2().getId());
			employeeRecord.setDepartmentId(department.getId());
			employeeRecord.setDepartmentRole(DepartmentRole.TECHNICAL_MANAGER.name());
			employeeDao.updateRecord(employeeRecord);
		}
		if (department.getTechManager3().getId() > -1) {
			EmployeeRecord employeeRecord = employeeDao.findOne(department.getTechManager3().getId());
			employeeRecord.setDepartmentId(department.getId());
			employeeRecord.setDepartmentRole(DepartmentRole.TECHNICAL_MANAGER.name());
			employeeDao.updateRecord(employeeRecord);
		}

	}
}
