/**
 * 
 */
package com.it218.AttendanceSystem.services;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.jooq.maven.postgres.tables.records.EmployeeRecord;
import org.jooq.maven.postgres.tables.records.RequestPassRecord;
import org.jooq.maven.postgres.tables.records.TeamEmployeeRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.it218.AttendanceSystem.dao.EmployeeDAO;
import com.it218.AttendanceSystem.dao.RequestPassDAO;
import com.it218.AttendanceSystem.dao.TeamEmployeeDAO;
import com.it218.AttendanceSystem.dto.RequestPassDTO;
import com.it218.AttendanceSystem.util.enums.DepartmentRole;
import com.it218.AttendanceSystem.util.enums.RequestPassStatus;
import com.it218.AttendanceSystem.util.mapper.ObjectMapper;

/**
 * @author reneir.val.t.perez
 *
 */
@Service
@Transactional
public class RequestPassMgtService {

	@Autowired
	private EmployeeDAO employeeDao;
	
	@Autowired
	private RequestPassDAO requestPassDao;
	
	@Autowired
	private TeamEmployeeDAO teamEmployeeDao;
	
	public List<RequestPassDTO> getAll() {
		List<RequestPassDTO> result = new ArrayList<>(); 
		for (RequestPassRecord record : requestPassDao.findAll()) {
			RequestPassDTO pass = ObjectMapper.toRequestPassDTO(record);
			result.add(pass);
		}
		return result;
	}
	
	public List<RequestPassDTO> getByEmployeeId(Integer employeeId, Optional<LocalDate> startDate) {
		List<RequestPassDTO> result = new ArrayList<>();
		EmployeeRecord employee = employeeDao.findOne(employeeId);
		ObjectMapper.setEmployeeDao(employeeDao);
		
		switch (DepartmentRole.valueOf(employee.getDepartmentRole())) {
		
			case ADMINISTRATOR:
				//Get all employees
				List<EmployeeRecord> allEmployees = employeeDao.findAll();
				for (EmployeeRecord record : allEmployees) {
					
					//Get all active requests by all subordinates
					List<RequestPassRecord> requests = new ArrayList<>(); 
					requests = requestPassDao.findAllRequestsTodayByRequestorId(record.getId(), startDate);
					for (RequestPassRecord request : requests) {
						
						//Add request information to result List
						result.add(ObjectMapper.toRequestPassDTO(request));
					}
				}
				break;
		
			case DEPARTMENT_MANAGER:
				//Get all employees managed by this employee
				List<EmployeeRecord> subordinates = employeeDao.findAllByDepartmentId(employee.getDepartmentId());
				for (EmployeeRecord subordinate : subordinates) {
					
					//Get all active requests by all subordinates
					List<RequestPassRecord> requests = requestPassDao.findAllRequestsTodayByRequestorId(subordinate.getId(), startDate);
					for (RequestPassRecord request : requests) {
						
						//Add request information to result List
						result.add(ObjectMapper.toRequestPassDTO(request));
					}
				}
				break;
			case TECHNICAL_MANAGER:
				//Get all teams managed by this employee
				List<TeamEmployeeRecord> techManagerTeams = teamEmployeeDao.findByMemberId(employeeId);
				for (TeamEmployeeRecord team : techManagerTeams) {
					
					//Get all team members managed by this team
					List<TeamEmployeeRecord> teamMembers = teamEmployeeDao.findByTeamId(team.getTeamId());
					for (TeamEmployeeRecord member : teamMembers) {
						
						//Get all active requests by all subordinates
						List<RequestPassRecord> requests = requestPassDao.findAllRequestsTodayByRequestorId(member.getMemberId(), startDate);
						for (RequestPassRecord request : requests) {
							
							//Add request information to result List
							result.add(ObjectMapper.toRequestPassDTO(request));
						}
					}
				}
				
				break;
			case TEAM_LEADER:
				//Get all teams managed by this employee
				List<TeamEmployeeRecord> teamLeaderTeams = teamEmployeeDao.findByMemberId(employeeId);
				for (TeamEmployeeRecord team : teamLeaderTeams) {
					
					//Get all team members managed by this team
					List<TeamEmployeeRecord> teamMembers = teamEmployeeDao.findByTeamId(team.getTeamId());
					for (TeamEmployeeRecord member : teamMembers) {
						
						//Get all active requests by all subordinates
						List<RequestPassRecord> requests = requestPassDao.findAllRequestsTodayByRequestorId(member.getMemberId(), startDate);
						for (RequestPassRecord request : requests) {
							
							//Add request information to result List
							result.add(ObjectMapper.toRequestPassDTO(request));
						}
					}
				}
				
				break;
			case TEAM_MEMBER:
				//Get all active requests by all subordinates
				List<RequestPassRecord> requests = requestPassDao.findAllRequestsTodayByRequestorId(employeeId, startDate);
				for (RequestPassRecord request : requests) {
					
					//Add request information to result List
					result.add(ObjectMapper.toRequestPassDTO(request));
				}
				break;
			default:
				//TODO: Return Error Message Exception
				break;
		}
				
		return result;
	}
	
	public RequestPassDTO approveRequestPass(Integer approverId, Integer requestPassId) throws Exception {
		RequestPassDTO result = null;
		EmployeeRecord approver = employeeDao.findOne(approverId);
				
		if (approver.getDepartmentRole().equals(DepartmentRole.DEPARTMENT_MANAGER.name())) {
			
			RequestPassRecord record = requestPassDao.findOne(requestPassId);
			if (record.getPassStatus() != RequestPassStatus.PENDING.getValue()) {
				throw new Exception("Request Pass already Approved / Used / Rejected.");
			}
			
			//Approve Request Pass
			record.setPassStatus(RequestPassStatus.APPROVED.getValue());
			record.setApproverId(approverId);
			
			result = ObjectMapper.toRequestPassDTO(
					requestPassDao.update(record));
		} else {
			//TODO: Return Error Message Exception
			throw new Exception("Not Authorized to Approve / Reject Request Passes.");
		}
		
		return result;
	}
	
	public RequestPassDTO rejectRequestPass(Integer approverId, Integer requestPassId) throws Exception {
		RequestPassDTO result = null;
		EmployeeRecord approver = employeeDao.findOne(approverId);
		
		if (approver.getDepartmentRole().equals(DepartmentRole.DEPARTMENT_MANAGER.name())) {
			
			RequestPassRecord record = requestPassDao.findOne(requestPassId);
			if (record.getPassStatus() != RequestPassStatus.PENDING.getValue()) {
				throw new Exception("Request Pass already Approved / Used / Rejected.");
			}
			
			//Approve Request Pass
			record.setPassStatus(RequestPassStatus.REJECTED.getValue());
			record.setApproverId(approverId);
			
			result = ObjectMapper.toRequestPassDTO(
					requestPassDao.update(record));
		} else {
			//TODO: Return Error Message Exception
			throw new Exception("Not Authorized to Approve / Reject Request Passes.");
		}
		
		return result;
	}
	
	public RequestPassDTO getById(Integer id) {
		RequestPassDTO result = null;
		RequestPassRecord record = requestPassDao.findOne(id);
		
		if (null != record) {
			result = ObjectMapper.toRequestPassDTO(record);
		} else {
			//TODO: Return Error Message Exception
		}
		
		return result;
	}
	
	public RequestPassDTO addRequestPass(Integer requestorId) {
		RequestPassRecord requestPass = new RequestPassRecord(
				null, 
				new Timestamp(System.currentTimeMillis()), 
				requestorId, 
				null, 
				RequestPassStatus.PENDING.getValue(), 
				null, 
				null);
		RequestPassRecord record = requestPassDao.addRecord(requestPass);
		return ObjectMapper.toRequestPassDTO(record);
	}
	
	public RequestPassDTO updateRequestPass(RequestPassDTO requestPass) {
		RequestPassRecord record = requestPassDao.update(ObjectMapper.toRequestPassRecord(requestPass));
		return ObjectMapper.toRequestPassDTO(record);
	}
	
	public void deleteById(Integer id) {
		requestPassDao.deleteById(id);
	}
}
