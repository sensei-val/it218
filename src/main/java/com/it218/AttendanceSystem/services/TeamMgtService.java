/**
 * 
 */
package com.it218.AttendanceSystem.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.jooq.maven.postgres.tables.records.DepartmentRecord;
import org.jooq.maven.postgres.tables.records.EmployeeRecord;
import org.jooq.maven.postgres.tables.records.TeamEmployeeRecord;
import org.jooq.maven.postgres.tables.records.TeamRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.it218.AttendanceSystem.dao.DepartmentDAO;
import com.it218.AttendanceSystem.dao.EmployeeDAO;
import com.it218.AttendanceSystem.dao.TeamDAO;
import com.it218.AttendanceSystem.dao.TeamEmployeeDAO;
import com.it218.AttendanceSystem.dto.EmployeeDTO;
import com.it218.AttendanceSystem.dto.TeamDTO;
import com.it218.AttendanceSystem.util.mapper.ObjectMapper;

/**
 * @author reneir.val.t.perez
 *
 */
@Service
@Transactional
public class TeamMgtService {

	@Autowired
	private TeamDAO teamDao;
	
	@Autowired
	private DepartmentDAO deptDao;

	@Autowired
	private EmployeeDAO employeeDao;
	
	@Autowired
	private TeamEmployeeDAO teamEmployeeDao;
	
	public List<TeamDTO> getAll() {
		List<TeamDTO> result = new ArrayList<>(); 
		for (TeamRecord record : teamDao.findAll()) {
			
			//Get list of team members
			List<EmployeeRecord> members = new ArrayList<>();
			teamEmployeeDao.findByTeamId(record.getId()).stream().forEach(teamMember -> {
				members.add(employeeDao.findOne(teamMember.getMemberId()));
			});
			
			//Get department where team belongs
			DepartmentRecord department = deptDao.findOne(record.getDeptId());
			
			TeamDTO teamDto = ObjectMapper.toTeamDTO(
					record,
					department,
					members);
			
			result.add(teamDto);
		}
		return result;
	}
	
	public TeamDTO getById(Integer id) {
		TeamDTO result = null;
		TeamRecord record = teamDao.findOne(id);
		
		if (null != record) {		
			
			//Get list of team members
			List<EmployeeRecord> members = new ArrayList<>();
			teamEmployeeDao.findByTeamId(record.getId()).stream().forEach(teamMember -> {
				members.add(employeeDao.findOne(teamMember.getMemberId()));
			});
			
			//Get department where team belongs
			DepartmentRecord department = deptDao.findOne(record.getDeptId());
			
			result = ObjectMapper.toTeamDTO(
					record,
					department,
					members);
		}
		
		return result;
	}
	
	public void addTeam(TeamDTO team) {
		for (EmployeeDTO teamMember : team.getMembers()) {
			teamEmployeeDao.addRecord(team.getId(), teamMember.getId(), teamMember.getDepartmentRole().name());
		}
		teamDao.addRecord(ObjectMapper.toTeamRecord(team));
	}
	
	public void updateTeam(TeamDTO team) {
				
		//Update Team Members
		List<TeamEmployeeRecord> beforeUpdateMembers = teamEmployeeDao.findByTeamId(team.getId());
		for (EmployeeDTO member : team.getMembers()) {
			//Check if member id is not present in current DB
			if (!beforeUpdateMembers.stream().filter(beforeMember -> 
					beforeMember.getMemberId().equals(member.getId())).findFirst().isPresent()) {
				teamEmployeeDao.addRecord(team.getId(), member.getId(), member.getDepartmentRole().name());
			}
		}
		for (TeamEmployeeRecord teamMember : beforeUpdateMembers) {
			//Check if team member id is not present in to be updated team
			if (!team.getMembers().stream().filter(member -> 
					member.getId().equals(teamMember.getMemberId())).findFirst().isPresent()) {
				teamEmployeeDao.deleteById(team.getId(), teamMember.getMemberId());
			}
		}
		teamDao.update(ObjectMapper.toTeamRecord(team));
	}
	
	public void deleteById(Integer teamId) {
		
		teamEmployeeDao.deleteByTeamId(teamId);
		
		teamDao.deleteById(teamId);
	}
}
