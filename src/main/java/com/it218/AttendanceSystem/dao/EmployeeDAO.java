/**
 * 
 */
package com.it218.AttendanceSystem.dao;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.StringJoiner;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record2;
import org.jooq.Record8;
import org.jooq.Result;
import org.jooq.maven.postgres.tables.records.EmployeeRecord;
import org.jooq.tools.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.it218.AttendanceSystem.util.enums.DepartmentRole;

/**
 * @author reneir.val.t.perez
 *
 */
@Repository
public class EmployeeDAO {

	@Autowired
	private DSLContext create;
	
	private org.jooq.maven.postgres.tables.Employee employeeTable = 
				new org.jooq.maven.postgres.tables.Employee();
	private org.jooq.maven.postgres.tables.Department departmentTable = 
			new org.jooq.maven.postgres.tables.Department();
	private org.jooq.maven.postgres.tables.Team teamTable = 
			new org.jooq.maven.postgres.tables.Team();
	
	public Result<EmployeeRecord> findAll() {
		Result<EmployeeRecord> result = create.selectFrom(employeeTable)
				.orderBy(employeeTable.UPDATED_DATE.desc()).fetch();
		return result;
	}
	
	public Result<EmployeeRecord> findAllEmployeesByRole(DepartmentRole role) {
		Result<EmployeeRecord> result = create.selectFrom(employeeTable)
				.where(employeeTable.DEPARTMENT_ROLE.eq(role.name()))
				.fetch();
		return result;
	}
	
	public Result<Record8<Integer, String, String, Integer, String, Integer, String, String>> search(
			String employeeInfo, Integer deptId, Integer teamId) {
		
		Condition condition1;
		if(!StringUtils.isBlank(employeeInfo)) {
			Condition condition0 = employeeTable.ID.eq(Integer.parseInt(employeeInfo));
			condition1 = condition0			
					.or(employeeTable.FIRST_NAME.likeIgnoreCase(("%").concat(employeeInfo).concat("%")))
					.or(employeeTable.LAST_NAME.likeIgnoreCase(("%").concat(employeeInfo).concat("%")));
		} else {
			condition1 = (employeeTable.FIRST_NAME.likeIgnoreCase(("%").concat(employeeInfo).concat("%")))
					.or(employeeTable.LAST_NAME.likeIgnoreCase(("%").concat(employeeInfo).concat("%"))); 
		}
		Condition condition2 = employeeTable.DEPARTMENT_ID.eq(deptId);
		Condition condition3 = teamTable.ID.eq(teamId);
		
		Result<Record8<Integer, String, String, Integer, String, Integer, String, String>> result = 
			create
				.select(employeeTable.ID,
						employeeTable.FIRST_NAME,
						employeeTable.LAST_NAME,
						departmentTable.ID,
						departmentTable.DEPT_NAME,
						teamTable.ID,
						teamTable.PROJECT_NAME,
						employeeTable.DEPARTMENT_ROLE
					)
				.from(employeeTable)
				.join(departmentTable)
					.on(employeeTable.DEPARTMENT_ID.eq(departmentTable.ID))
				.join(teamTable)
					.on(departmentTable.ID.eq(teamTable.DEPT_ID))
				.where(condition1
					.or(condition2)
					.or(condition3)
					.or(condition1.and(condition2))
					.or(condition1.and(condition3))
					.or(condition2.and(condition3))
					.or(condition1.and(condition2).and(condition3))
					)
				.fetch();
		
		return result;
	}
	
	public EmployeeRecord findOne(Integer id) {
		EmployeeRecord result = create.selectFrom(employeeTable)
				.where(employeeTable.ID.eq(id))
				.fetchOne();
		return result;
	}
	
	public String findEmployeeName(Integer id) {
		String result = null;
		Record2<String,String> records = create
				.select(employeeTable.FIRST_NAME, employeeTable.LAST_NAME)
				.from(employeeTable)
				.where(employeeTable.ID.eq(id))
				.fetchOne();
		if(records != null) {
			StringJoiner join = new StringJoiner(" ");
			join.add(records.getValue(employeeTable.FIRST_NAME));
			join.add(records.getValue(employeeTable.LAST_NAME));
			result = join.toString();
		}
		return result;
	}
	
	public Result<EmployeeRecord> findByDeptIdAndRole(Integer departmentId, DepartmentRole role) {
		Result<EmployeeRecord> result = create.selectFrom(employeeTable)
				.where(employeeTable.DEPARTMENT_ID.eq(departmentId))
				.and(employeeTable.DEPARTMENT_ROLE.eq(role.name()))
				.fetch();
		return result;
	}
	
	public Result<EmployeeRecord> findByDeptIdAndRole(Integer departmentId, List<DepartmentRole> roles) {
		Result<EmployeeRecord> result = create.selectFrom(employeeTable)
				.where(employeeTable.DEPARTMENT_ID.eq(departmentId))
				.and(employeeTable.DEPARTMENT_ROLE.in(roles))
				.fetch();
		return result;
	}
	
	public Result<EmployeeRecord> findAllByDepartmentId(Integer departmentId) {
		Result<EmployeeRecord> result = create.selectFrom(employeeTable)
				.where(employeeTable.DEPARTMENT_ID.eq(departmentId))
				.fetch();
		return result;
	}
		
	public EmployeeRecord addRecord(EmployeeRecord employee) {
		EmployeeRecord result = null;
		result = create.insertInto(
					employeeTable, 
					employeeTable.PASSWORD,
					employeeTable.FIRST_NAME,
					employeeTable.LAST_NAME,
					employeeTable.ADDRESS,
					employeeTable.BIRTHDAY,
					employeeTable.DEPARTMENT_ID,
					employeeTable.DEPARTMENT_ROLE,
					employeeTable.CREATED_DATE,
					employeeTable.UPDATED_DATE)
				.values(
					employee.getPassword(),
					employee.getFirstName(),
					employee.getLastName(),
					employee.getAddress(),
					employee.getBirthday(),
					employee.getDepartmentId(),
					employee.getDepartmentRole(),
					Timestamp.from(Instant.now()),
					Timestamp.from(Instant.now()))
				.returning()
				.fetchOne();	
		
		return result;
	}
	
	public EmployeeRecord updateRecord(EmployeeRecord employee) {
		EmployeeRecord result = null;
		result = create.update(employeeTable)
						.set(employeeTable.PASSWORD, employee.getPassword())
						.set(employeeTable.FIRST_NAME, employee.getFirstName())
						.set(employeeTable.LAST_NAME, employee.getLastName())
						.set(employeeTable.ADDRESS, employee.getAddress())
						.set(employeeTable.BIRTHDAY, employee.getBirthday())
						.set(employeeTable.DEPARTMENT_ID, employee.getDepartmentId())
						.set(employeeTable.DEPARTMENT_ROLE, employee.getDepartmentRole())
						.set(employeeTable.UPDATED_DATE, Timestamp.from(Instant.now()))
					.where(employeeTable.ID.eq(employee.getId()))
					.returning()
					.fetchOne();
		
		return result;
	}
	
	public void deleteById(Integer id) {
		create.deleteFrom(employeeTable)
				.where(employeeTable.ID.eq(id))
				.execute();
	}
}
