/**
 * 
 */
package com.it218.AttendanceSystem.dao;

import java.sql.Timestamp;
import java.time.Instant;

import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.maven.postgres.tables.records.DepartmentRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * @author reneir.val.t.perez
 *
 */
@Repository
public class DepartmentDAO {

	@Autowired
	private DSLContext create;
	
	private org.jooq.maven.postgres.tables.Department departmentTable = 
				new org.jooq.maven.postgres.tables.Department();
	
	public Result<DepartmentRecord> findAll() {
		Result<DepartmentRecord> result = create.selectFrom(departmentTable)
				.orderBy(departmentTable.UPDATED_DATE.desc()).fetch();
		return result;
	}
	
	public DepartmentRecord findOne(Integer id) {
		DepartmentRecord result = create.selectFrom(departmentTable)
				.where(departmentTable.ID.eq(id))
				.fetchOne();
		return result;
	}
	
	public DepartmentRecord findByDeptCode(String deptCode) {
		DepartmentRecord result = create.selectFrom(departmentTable)
				.where(departmentTable.DEPT_CODE.eq(deptCode))
				.fetchOne();
		return result;
	}
	
	public DepartmentRecord addRecord(DepartmentRecord department) {
		DepartmentRecord result = null;
		result = create.insertInto(
					departmentTable, 
					departmentTable.DEPT_CODE, 
					departmentTable.DEPT_NAME, 
					departmentTable.CREATED_DATE,
					departmentTable.UPDATED_DATE)
				.values(
					department.getDeptCode(),
					department.getDeptName(),
					Timestamp.from(Instant.now()),
					Timestamp.from(Instant.now()))
				.returning(
					departmentTable.ID, 
					departmentTable.DEPT_CODE, 
					departmentTable.DEPT_NAME)
				.fetchOne();	
		
		return result;
	}
	
	public DepartmentRecord update(DepartmentRecord department) {
		DepartmentRecord result = null;
		result = create.update(departmentTable)
						.set(departmentTable.DEPT_CODE, department.getDeptCode())
						.set(departmentTable.DEPT_NAME, department.getDeptName())
						.set(departmentTable.UPDATED_DATE, Timestamp.from(Instant.now()))
					.where(departmentTable.ID.eq(department.getId()))
					.returning(
							departmentTable.ID, 
							departmentTable.DEPT_CODE, 
							departmentTable.DEPT_NAME)
					.fetchOne();
		
		return result;
	}
	
	public void deleteById(Integer id) {
		create.deleteFrom(departmentTable)
				.where(departmentTable.ID.eq(id))
				.execute();
	}
}
