/**
 * 
 */
package com.it218.AttendanceSystem.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

import org.jooq.Context;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record3;
import org.jooq.Result;
import org.jooq.impl.CustomField;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.maven.postgres.tables.records.AttendanceRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author reneir.val.t.perez
 *
 */
@Repository
public class AttendanceDAO {

	@Autowired
	private DSLContext create;
	
	private org.jooq.maven.postgres.tables.Attendance attendanceTable = 
				new org.jooq.maven.postgres.tables.Attendance();
	
	public Result<AttendanceRecord> findAll() {
		Result<AttendanceRecord> result = create.selectFrom(attendanceTable)
				.orderBy(attendanceTable.UPDATED_DATE.desc()).fetch();
		return result;
	}
	
	public Result<AttendanceRecord> findAllByEmployeeId(Integer employeeId) {
		Result<AttendanceRecord> result = create.selectFrom(attendanceTable)
				.where(attendanceTable.EMPLOYEE_ID.eq(employeeId))
				.orderBy(attendanceTable.DATE.desc())
				.fetch();
		return result;
	}
	
	public Result<AttendanceRecord> findAllByEmployeeIdAndDate(Integer employeeId, LocalDate date) {
		Result<AttendanceRecord> result = create.selectFrom(attendanceTable)
				.where(attendanceTable.EMPLOYEE_ID.eq(employeeId))
				.and(attendanceTable.DATE.greaterOrEqual(Timestamp.valueOf(date.atStartOfDay())))
				.fetch();
		return result;
	}
	
	public Result<AttendanceRecord> findDAllByDateWithNoTimeOut(Integer employeeId, LocalDate date) {
		Result<AttendanceRecord> result = create.selectFrom(attendanceTable)
				.where(attendanceTable.ID.eq(employeeId))
				.and(attendanceTable.DATE.greaterOrEqual(Timestamp.valueOf(date.atStartOfDay())))
				.and(attendanceTable.TIMEIN.isNotNull())
				.and(attendanceTable.TIMEOUT.isNull())
				.orderBy(attendanceTable.UPDATED_DATE.desc())
				.fetch();
		return result;
	}
	
	public AttendanceRecord findOne(Integer id) {
		AttendanceRecord result = create.selectFrom(attendanceTable)
				.where(attendanceTable.ID.eq(id))
				.fetchOne();
		return result;
	}
	
	public AttendanceRecord addRecord(AttendanceRecord attendance) {
		AttendanceRecord result = null;
		result = create.insertInto(
					attendanceTable, 
					attendanceTable.EMPLOYEE_ID, 
					attendanceTable.DATE, 
					attendanceTable.TIMEIN, 
					attendanceTable.TIMEOUT, 
					attendanceTable.CREATED_DATE,
					attendanceTable.UPDATED_DATE)
				.values(
					attendance.getEmployeeId(),
					attendance.getDate(),
					attendance.getTimein(),
					attendance.getTimeout(),
					Timestamp.from(Instant.now()),
					Timestamp.from(Instant.now()))
				.returning()
				.fetchOne();
		
		return result;
	}
	
	public AttendanceRecord update(AttendanceRecord attendance) {
		AttendanceRecord result = null;
		result = create.update(attendanceTable)
						.set(attendanceTable.EMPLOYEE_ID, attendance.getEmployeeId())
						.set(attendanceTable.DATE, attendance.getDate())
						.set(attendanceTable.TIMEIN, attendance.getTimein())
						.set(attendanceTable.TIMEOUT, attendance.getTimeout())
						.set(attendanceTable.UPDATED_DATE, Timestamp.from(Instant.now()))
					.where(attendanceTable.ID.eq(attendance.getId()))
					.returning()
					.fetchOne();
		
		return result;
	}
	
	public AttendanceRecord updateTimeOut(AttendanceRecord attendance) {
		AttendanceRecord result = null;
		result = create.update(attendanceTable)
						.set(attendanceTable.DATE, attendance.getDate())
						.set(attendanceTable.TIMEOUT, attendance.getTimeout())
						.set(attendanceTable.UPDATED_DATE, Timestamp.from(Instant.now()))
					.where(attendanceTable.ID.eq(attendance.getId()))
					.returning()
					.fetchOne();
		
		return result;
	}
	
	public void deleteById(Integer id) {
		create.deleteFrom(attendanceTable)
				.where(attendanceTable.ID.eq(id))
				.execute();
	}
	
	public Result<Record3<Integer, Timestamp, BigDecimal>> summary(List<Integer> employeeIds, LocalDate startDate) {
		Result<Record3<Integer, Timestamp, BigDecimal>> result = create.select(
				attendanceTable.EMPLOYEE_ID.as("EMPLOYEE_ID"),
				attendanceTable.DATE.as("DATE"),
				sumTimestampDiff(attendanceTable.TIMEOUT, attendanceTable.TIMEIN).as("WORK_HOURS"))
			.from(attendanceTable)
			.where(attendanceTable.EMPLOYEE_ID.in(employeeIds))
			.groupBy(attendanceTable.EMPLOYEE_ID, attendanceTable.DATE)
			.having(attendanceTable.DATE.greaterOrEqual(Timestamp.valueOf(startDate.atStartOfDay())))
			.orderBy(attendanceTable.DATE.asc())
			.fetch();
		return result;
	}
	
	@SuppressWarnings("serial")
	private Field<BigDecimal> sumTimestampDiff(
		    final Field<Timestamp> timeOut,
		    final Field<Timestamp> timeIn
		) {
		    return new CustomField<BigDecimal>("sum", SQLDataType.NUMERIC) {
				@Override
				public void accept(Context<?> ctx) {
					ctx.visit(DSL.sql("sum(1000 * extract('epoch' from ({0} - {1})))", timeOut, timeIn));
				}
		    };
		}

}
