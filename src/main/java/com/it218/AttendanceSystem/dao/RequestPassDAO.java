/**
 * 
 */
package com.it218.AttendanceSystem.dao;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Optional;

import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.maven.postgres.tables.records.RequestPassRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.it218.AttendanceSystem.util.enums.RequestPassStatus;

/**
 * @author reneir.val.t.perez
 *
 */
@Repository
public class RequestPassDAO {

	@Autowired
	private DSLContext create;
	
	private org.jooq.maven.postgres.tables.RequestPass requestPassTable = 
				new org.jooq.maven.postgres.tables.RequestPass();
	
	public Result<RequestPassRecord> findAll() {
		Result<RequestPassRecord> result = create.selectFrom(requestPassTable)
				.orderBy(requestPassTable.UPDATED_DATE.desc()).fetch();
		return result;
	}
	
	public Result<RequestPassRecord> findAllRequestsTodayByRequestorId(Integer requestorId, Optional<LocalDate> date) {
		Result<RequestPassRecord> result;
		if (date.isPresent()) {
			result = create.selectFrom(requestPassTable)
					.where(requestPassTable.REQUESTOR_ID.eq(requestorId))
					.and(requestPassTable.DATE.greaterOrEqual(
							Timestamp.valueOf(date.get().atStartOfDay())))
					.orderBy(requestPassTable.UPDATED_DATE.desc()).fetch();
		} else {
			result = create.selectFrom(requestPassTable)
					.where(requestPassTable.REQUESTOR_ID.eq(requestorId))
					.orderBy(requestPassTable.UPDATED_DATE.desc()).fetch();
		}
		return result;
	}
	
	public Result<RequestPassRecord> findAllRequestsByRequestorIdAndDateAndStatus(Integer requestorId, LocalDate date, RequestPassStatus passStatus) {
		Result<RequestPassRecord> result = create.selectFrom(requestPassTable)
				.where(requestPassTable.REQUESTOR_ID.eq(requestorId))
				.and(requestPassTable.PASS_STATUS.eq(passStatus.getValue()))
				.and(requestPassTable.DATE.greaterOrEqual(
						Timestamp.valueOf(date.atStartOfDay())))
				.orderBy(requestPassTable.UPDATED_DATE.desc()).fetch();
		return result;
	}
	
	public Result<RequestPassRecord> findAllRequestsByApproverId(Integer approverId) {
		Result<RequestPassRecord> result = create.selectFrom(requestPassTable)
				.where(requestPassTable.APPROVER_ID.eq(approverId))
				.fetch();
		return result;
	}
	
	public RequestPassRecord findOne(Integer id) {
		RequestPassRecord result = create.selectFrom(requestPassTable)
				.where(requestPassTable.ID.eq(id))
				.fetchOne();
		return result;
	}
	
	public RequestPassRecord addRecord(RequestPassRecord requestPass) {
		RequestPassRecord result = null;
		result = create.insertInto(
					requestPassTable, 
					requestPassTable.DATE,
					requestPassTable.REQUESTOR_ID,
					requestPassTable.PASS_STATUS,
					requestPassTable.CREATED_DATE,
					requestPassTable.UPDATED_DATE)
				.values(
					requestPass.getDate(),
					requestPass.getRequestorId(),
					requestPass.getPassStatus(),
					Timestamp.from(Instant.now()),
					Timestamp.from(Instant.now()))
				.returning()
				.fetchOne();
		
		return result;
	}
	
	public RequestPassRecord update(RequestPassRecord requestPass) {
		RequestPassRecord result = null;
		result = create.update(requestPassTable)
						.set(requestPassTable.DATE, requestPass.getDate())
						.set(requestPassTable.REQUESTOR_ID, requestPass.getRequestorId())
						.set(requestPassTable.APPROVER_ID, requestPass.getApproverId())
						.set(requestPassTable.PASS_STATUS, requestPass.getPassStatus())
						.set(requestPassTable.UPDATED_DATE, Timestamp.from(Instant.now()))
					.where(requestPassTable.ID.eq(requestPass.getId()))
					.returning()
					.fetchOne();
		
		return result;
	}
	
	public void deleteById(Integer id) {
		create.deleteFrom(requestPassTable)
				.where(requestPassTable.ID.eq(id))
				.execute();
	}
}
