/**
 * 
 */
package com.it218.AttendanceSystem.dao;

import java.sql.Timestamp;
import java.time.Instant;

import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.maven.postgres.tables.records.TeamRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author reneir.val.t.perez
 *
 */
@Repository
public class TeamDAO {

	@Autowired
	private DSLContext create;
	
	private org.jooq.maven.postgres.tables.Team teamTable = 
				new org.jooq.maven.postgres.tables.Team();
	
	public Result<TeamRecord> findAll() {
		Result<TeamRecord> result = create.selectFrom(teamTable)
				.orderBy(teamTable.UPDATED_DATE.desc()).fetch();
		return result;
	}
	
	public TeamRecord findOne(Integer id) {
		TeamRecord result = create.selectFrom(teamTable)
				.where(teamTable.ID.eq(id))
				.fetchOne();
		return result;
	}
		
	public TeamRecord addRecord(TeamRecord team) {
		TeamRecord result = null;
		result = create.insertInto(
					teamTable, 
					teamTable.DEPT_ID,
					teamTable.PROJECT_NAME,
					teamTable.CREATED_DATE,
					teamTable.UPDATED_DATE)
				.values(
					team.getDeptId(),
					team.getProjectName(),
					Timestamp.from(Instant.now()),
					Timestamp.from(Instant.now()))
				.returning()
				.fetchOne();	
		
		return result;
	}
	
	public TeamRecord update(TeamRecord team) {
		TeamRecord result = null;
		result = create.update(teamTable)
						.set(teamTable.DEPT_ID, team.getDeptId())
						.set(teamTable.PROJECT_NAME, team.getProjectName())
						.set(teamTable.UPDATED_DATE, Timestamp.from(Instant.now()))
					.where(teamTable.ID.eq(team.getId()))
					.returning()
					.fetchOne();
		
		return result;
	}
	
	public void deleteById(Integer id) {
		create.deleteFrom(teamTable)
				.where(teamTable.ID.eq(id))
				.execute();
	}
}
