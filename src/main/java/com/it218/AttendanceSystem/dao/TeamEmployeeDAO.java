/**
 * 
 */
package com.it218.AttendanceSystem.dao;

import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.maven.postgres.tables.records.TeamEmployeeRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author reneir.val.t.perez
 *
 */
@Repository
public class TeamEmployeeDAO {

	@Autowired
	private DSLContext create;
	
	private org.jooq.maven.postgres.tables.TeamEmployee teamEmployeeTable = 
				new org.jooq.maven.postgres.tables.TeamEmployee();
	
	public Result<TeamEmployeeRecord> findAll() {
		Result<TeamEmployeeRecord> result = create.selectFrom(teamEmployeeTable).fetch();
		return result;
	}
	
	public TeamEmployeeRecord findByTeamIdAndMemberId(Integer teamId, Integer memberId) {
		TeamEmployeeRecord result = create.selectFrom(teamEmployeeTable)
				.where(teamEmployeeTable.TEAM_ID.eq(teamId))
				.and(teamEmployeeTable.MEMBER_ID.eq(memberId))
				.fetchOne();
		return result;
	}
	
	public Result<TeamEmployeeRecord> findByTeamId(Integer teamId) {
		Result<TeamEmployeeRecord> result = create.selectFrom(teamEmployeeTable)
				.where(teamEmployeeTable.TEAM_ID.eq(teamId))
				.fetch();
		return result;
	}
	
	public Result<TeamEmployeeRecord> findByMemberId(Integer memberId) {
		Result<TeamEmployeeRecord> result = create.selectFrom(teamEmployeeTable)
				.where(teamEmployeeTable.MEMBER_ID.eq(memberId))
				.fetch();
		return result;
	}
	
	public Result<TeamEmployeeRecord> findByMemberIdAndRole(Integer memberId, String role) {
		Result<TeamEmployeeRecord> result = create.selectFrom(teamEmployeeTable)
				.where(teamEmployeeTable.MEMBER_ID.eq(memberId))
				.and(teamEmployeeTable.ROLE.eq(role))
				.fetch();
		return result;
	}
		
	public TeamEmployeeRecord addRecord(Integer teamId, Integer memberId, String role) {
		TeamEmployeeRecord result = null;
		result = create.insertInto(
					teamEmployeeTable, 
					teamEmployeeTable.TEAM_ID,
					teamEmployeeTable.MEMBER_ID,
					teamEmployeeTable.ROLE)
				.values(
					teamId,
					memberId, 
					role)
				.returning()
				.fetchOne();	
		
		return result;
	}
	
	public TeamEmployeeRecord updateRecord(TeamEmployeeRecord teamEmployee) {
		TeamEmployeeRecord result = null;
		result = create.update(teamEmployeeTable)
						.set(teamEmployeeTable.ROLE, teamEmployee.getRole())
					.where(
						teamEmployeeTable.TEAM_ID.eq(teamEmployee.getTeamId())
					.and(
						teamEmployeeTable.MEMBER_ID.eq(teamEmployee.getMemberId())))
					.returning()
					.fetchOne();
		
		return result;
	}
	
	public void deleteByTeamId(Integer teamId) {
		create.deleteFrom(teamEmployeeTable)
				.where(
					teamEmployeeTable.TEAM_ID.eq(teamId))
				.execute();
	}
	
	public void deleteByMemberId(Integer memberId) {
		create.deleteFrom(teamEmployeeTable)
				.where(
					teamEmployeeTable.MEMBER_ID.eq(memberId))
				.execute();
	}
	
	public void deleteById(Integer teamId, Integer memberId) {
		create.deleteFrom(teamEmployeeTable)
				.where(
					teamEmployeeTable.TEAM_ID.eq(teamId))
				.and(
					teamEmployeeTable.MEMBER_ID.eq(memberId))
				.execute();
	}
}
