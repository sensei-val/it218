/**
 * 
 */
package com.it218.AttendanceSystem.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.it218.AttendanceSystem.dto.AttendanceDTO;
import com.it218.AttendanceSystem.dto.TimeSummaryDTO;
import com.it218.AttendanceSystem.services.AttendanceMgtService;

/**
 * @author reneir.val.t.perez
 *
 */
@RestController
@RequestMapping("/attendance")
public class AttendanceMgtController {

	@Autowired
	private AttendanceMgtService service;
	
	@GetMapping
	public List<AttendanceDTO> getAll() {
		return service.getAll();
	}
	
	@GetMapping("/{employeeId}")
	public List<AttendanceDTO> getAllByEmployeeId(@PathVariable Integer employeeId) {
		return service.getAll();
	}
	
	@GetMapping("/review")
	public Set<TimeSummaryDTO> getAllByReviewerId(@RequestParam Integer reviewerId, @RequestParam String startDate) {
		return service.getSummaryLogs(reviewerId, LocalDate.parse(startDate));
	}
	
	@GetMapping("/todayAttendanceId/{employeeId}")
	public Integer getLatestAttendanceRecordId(@PathVariable Integer employeeId) {
		return service.getLatestAttendanceRecordId(employeeId, LocalDate.now());
	}
	
	@PostMapping("/timeIn")
	public AttendanceDTO createNew(@Valid @RequestBody AttendanceDTO attendance) throws Exception {
		attendance.setDate(LocalDate.now());
		return service.inputTimeIn(attendance);
	}
	
	@PutMapping("/timeOut")
	public AttendanceDTO update(@Valid @RequestBody AttendanceDTO attendance) {
		attendance.setDate(LocalDate.now());
		return service.inputTimeOut(attendance);
	}
		
	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable Integer id) {
		service.deleteById(id);
	}
}
