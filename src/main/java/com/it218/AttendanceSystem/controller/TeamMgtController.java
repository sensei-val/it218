/**
 * 
 */
package com.it218.AttendanceSystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.it218.AttendanceSystem.dto.TeamDTO;
import com.it218.AttendanceSystem.services.TeamMgtService;

/**
 * @author reneir.val.t.perez
 *
 */
@RestController
@RequestMapping("/teams")
public class TeamMgtController {

	@Autowired
	public TeamMgtService service;
	
	@GetMapping
	public List<TeamDTO> getAll() {
		return service.getAll();
	}
	
	@GetMapping("/{id}")
	public TeamDTO getById(@PathVariable Integer id) {
		return service.getById(id);
	}
	
	@PostMapping
	public void createNew(@Valid @RequestBody TeamDTO team) {
		service.addTeam(team);
	}
	
	@PutMapping
	public void update(@Valid @RequestBody TeamDTO team) {
		service.updateTeam(team);
	}
	
	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable Integer id) {
		service.deleteById(id);
	}
}
