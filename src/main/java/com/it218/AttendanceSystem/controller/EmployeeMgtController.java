/**
 * 
 */
package com.it218.AttendanceSystem.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.jooq.tools.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.it218.AttendanceSystem.dto.EmployeeDTO;
import com.it218.AttendanceSystem.dto.SearchResultsDTO;
import com.it218.AttendanceSystem.services.EmployeeMgtService;
import com.it218.AttendanceSystem.services.RequestPassMgtService;
import com.it218.AttendanceSystem.util.enums.DepartmentRole;

/**
 * @author reneir.val.t.perez
 *
 */
@RestController
@RequestMapping("/employees")
public class EmployeeMgtController {

	@Autowired
	public EmployeeMgtService service;

	@Autowired
	public RequestPassMgtService requestPassService;
	
	@GetMapping
	public List<EmployeeDTO> getAll() {
		return service.getAll();
	}
	
	@GetMapping("/profile/{employeeId}")
	public EmployeeDTO getById(@PathVariable Integer employeeId) {
		return service.getById(employeeId);
	}
	
	@GetMapping("/{roleTxt}")
	public List<EmployeeDTO> getEmployeesByRole(@PathVariable String roleTxt) throws Exception {
		DepartmentRole role = null;
		switch(roleTxt) {
			case "ADMIN" : role = DepartmentRole.ADMINISTRATOR; break;
			case "DEPT" : role = DepartmentRole.DEPARTMENT_MANAGER; break;
			case "TECH" : role = DepartmentRole.TECHNICAL_MANAGER; break;
			case "TLEAD" : role = DepartmentRole.TEAM_LEADER; break;
			case "MEMBER" : role = DepartmentRole.TEAM_MEMBER; break;
		}
		return service.getEmployeesByRole(role);
	}
	
	@GetMapping("/search")
	public List<SearchResultsDTO> search(@RequestParam Optional<String> employeeIdName,
			@RequestParam Optional<Integer> deptId,
			@RequestParam Optional<Integer> teamId) {
		List<SearchResultsDTO> results = new ArrayList<>();
		String parameter1 = StringUtils.EMPTY;
		Integer parameter2 = -1;
		Integer parameter3 = -1;
		
		if (employeeIdName.isPresent()) {
			parameter1 = employeeIdName.get();
		}
		if (deptId.isPresent()) {
			parameter2 = deptId.get();
		}
		if (teamId.isPresent()) {
			parameter3 = teamId.get();
		}
		results = service.search(parameter1, parameter2, parameter3);
		return results;
	}
	
	@PostMapping
	public EmployeeDTO createNew(@Valid @RequestBody EmployeeDTO employee) {
		return service.addEmployee(employee);
	}
	
	@PutMapping
	public EmployeeDTO update(@Valid @RequestBody EmployeeDTO employee) {
		return service.updateEmployee(employee);
	}
	
	@PutMapping("/role/department")
	public EmployeeDTO assignDepartmentRole(@RequestParam Integer employeeId, @RequestParam Integer departmentId, @RequestParam DepartmentRole role) {
		return service.assignEmployeeDeptRole(employeeId, departmentId, role);
	}
	
	@PutMapping("/role/team")
	public EmployeeDTO assignTeamRole(@RequestParam Integer employeeId, @RequestParam Integer teamId, @RequestParam DepartmentRole role) {
		return service.assignEmployeeTeamRole(employeeId, teamId, role);
	}
	
	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable Integer id) {
		service.deleteById(id);
	}
}
