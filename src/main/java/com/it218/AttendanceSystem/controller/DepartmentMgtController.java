/**
 * 
 */
package com.it218.AttendanceSystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.it218.AttendanceSystem.dto.DepartmentDTO;
import com.it218.AttendanceSystem.services.DepartmentMgtService;
import com.it218.AttendanceSystem.util.enums.DepartmentRole;

/**
 * @author reneir.val.t.perez
 *
 */
@RestController
@RequestMapping("/departments")
public class DepartmentMgtController {

	@Autowired
	public DepartmentMgtService service;
	
	@GetMapping
	public List<DepartmentDTO> getAll() {
		return service.getAll();
	}
	
	@GetMapping("/{departmentId}")
	public DepartmentDTO getByDepartmentId(@PathVariable Integer departmentId) {
		return service.getById(departmentId);
	}
	
	@GetMapping("/roles")
	public List<DepartmentRole> getAllDepartmentRoles() {
		return service.getAllDepartmentRoles();
	}
	
	@PostMapping
	public void createNew(@Valid @RequestBody DepartmentDTO department) {
		service.addDepartment(department);
	}
	
	@PutMapping
	public void update(@Valid @RequestBody DepartmentDTO department) {
		service.updateDepartment(department);
	}
		
	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable Integer id) {
		service.deleteById(id);
	}
}
