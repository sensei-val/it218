/**
 * 
 */
package com.it218.AttendanceSystem.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.it218.AttendanceSystem.dto.RequestPassDTO;
import com.it218.AttendanceSystem.services.RequestPassMgtService;

/**
 * @author reneir.val.t.perez
 *
 */
@RestController
@RequestMapping("/requests")
public class RequestPassMgtController {

	@Autowired
	public RequestPassMgtService requestPassService;

	@GetMapping("/{employeeId}")
	public List<RequestPassDTO> getAllRequestPassesById(@PathVariable Integer employeeId) {
		return requestPassService.getByEmployeeId(employeeId, Optional.empty());
	}
	
	@GetMapping("/today/{employeeId}")
	public List<RequestPassDTO> getRequestPassesToday(@PathVariable Integer employeeId) {
		return requestPassService.getByEmployeeId(employeeId, Optional.of(LocalDate.now()));
	}
	
	@PostMapping()
	public RequestPassDTO requestPass(@RequestParam Integer requestorId) {
		return requestPassService.addRequestPass(requestorId);
	}
	
	@PutMapping("/approve")
	public RequestPassDTO approvePass(
			@RequestParam Integer requestPassId,
			@RequestParam Integer approverId) throws Exception {
		return requestPassService.approveRequestPass(approverId, requestPassId);
	}
	
	@PutMapping("/reject")
	public RequestPassDTO rejectPass(
			@RequestParam Integer requestPassId,
			@RequestParam Integer approverId) throws Exception {
		return requestPassService.rejectRequestPass(approverId, requestPassId);
	}
	
}
