package com.it218.AttendanceSystem.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jooq.tools.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.it218.AttendanceSystem.dto.EmployeeDTO;
import com.it218.AttendanceSystem.services.EmployeeMgtService;
import com.it218.AttendanceSystem.util.enums.DepartmentRole;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author reneir.val.t.perez
 *
 */
@Component
public class TokenAuthenticationService implements AuthenticationProvider {
	
	  static final long EXPIRATIONTIME = 864_000_000; // 10 days
	  static final String SECRET = "ThisIsASecret";
	  static final String TOKEN_PREFIX = "Bearer";
	  static final String HEADER_STRING = "Authorization";
	  
	  private final String ROLE_PREFIX = "ROLE_";
	  
	  @Autowired
	  private EmployeeMgtService employeeMgtService;

	  static void addAuthentication(HttpServletResponse res, String username, Collection<?> authorities) {
		Claims claims = Jwts.claims().setSubject(username);
	    claims.put("scopes", authorities.stream().map(s -> s.toString()).collect(Collectors.toList()));  
		  
	    String JWT = Jwts.builder()
	    	.setClaims(claims)
	        .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
	        .signWith(SignatureAlgorithm.HS512, SECRET)
	        .compact();
	    res.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
	  }

	  static Authentication getAuthentication(HttpServletRequest request) {
	    String token = request.getHeader(HEADER_STRING);
	    if (!StringUtils.isBlank(token)) {
	      // parse the token.
	      String user = Jwts.parser()
	          .setSigningKey(SECRET)
	          .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
	          .getBody()
	          .getSubject();

	      return user != null ?
	          new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList()) :
	          null;
	    }
	    return null;
	  }

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {        
        Integer userId = (Integer) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        EmployeeDTO user = employeeMgtService.getById(userId);
        if (user == null) {
        	throw new UsernameNotFoundException("Employee not found: " + userId);
        }
        
        if (!(user.getId().equals(userId) && 
        		user.getPassword().equals(password))) {
        	throw new BadCredentialsException("Authentication Failed. Employee ID or Password not valid.");
        }
        
//        if (!encoder.matches(password, user.getPassword())) {
//            throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
//        }

        if (user.getDepartmentRole() == null) {
        	throw new InsufficientAuthenticationException("User has no roles assigned");
        }

        List<GrantedAuthority> authorities = this.getAuthorities(user.getDepartmentRole());

        return new UsernamePasswordAuthenticationToken(userId, password, authorities);
    }

	@Override
	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}
	

	private List<GrantedAuthority> getAuthorities(DepartmentRole role) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + role.name()));
		return authorities;
	}
	
}
