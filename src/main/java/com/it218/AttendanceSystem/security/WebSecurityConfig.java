package com.it218.AttendanceSystem.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.it218.AttendanceSystem.util.enums.DepartmentRole;

/**
 * @author reneir.val.t.perez
 *
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private TokenAuthenticationService tokenAuthentication;
	
	@Override
	  protected void configure(HttpSecurity http) throws Exception {
	    http.csrf().disable().authorizeRequests()
	        .antMatchers("/").permitAll()

	        .antMatchers(HttpMethod.PUT, "/teams/*").hasAnyRole(
	        		DepartmentRole.ADMINISTRATOR.name(),
	        		DepartmentRole.DEPARTMENT_MANAGER.name(),
	        		DepartmentRole.TECHNICAL_MANAGER.name(),
	        		DepartmentRole.TEAM_LEADER.name())

	        .antMatchers(HttpMethod.POST, "/teams/*").hasAnyRole(
	        		DepartmentRole.ADMINISTRATOR.name(),
	        		DepartmentRole.DEPARTMENT_MANAGER.name(),
	        		DepartmentRole.TECHNICAL_MANAGER.name())

	        .antMatchers(HttpMethod.PUT, "/departments/*").hasAnyRole(
		    		DepartmentRole.ADMINISTRATOR.name(),
		    		DepartmentRole.DEPARTMENT_MANAGER.name())

	        .antMatchers(HttpMethod.POST, "/departments/*").hasRole(
	        		DepartmentRole.ADMINISTRATOR.name())
	        
	        .antMatchers(HttpMethod.POST, "/employees/*").hasRole(
	        		DepartmentRole.ADMINISTRATOR.name())
	        
	        .antMatchers(HttpMethod.PUT, "/employees/*").hasRole(
	        		DepartmentRole.ADMINISTRATOR.name())
	        
	        .antMatchers(HttpMethod.GET, "/bootstrap/**").permitAll()
	        .antMatchers(HttpMethod.GET, "/resources/**").permitAll()
	        .antMatchers(HttpMethod.GET, "/scripts/**").permitAll()
	        .antMatchers(HttpMethod.GET, "/templates/**").permitAll()
	        .antMatchers(HttpMethod.GET, "/favicon.ico").permitAll()
	        .antMatchers(HttpMethod.POST, "/login").permitAll()
	        .anyRequest().authenticated()
	        .and()
	        // We filter the api/login requests
	        .addFilterBefore(new JwtLoginFilter("/login", authenticationManager()),
	                UsernamePasswordAuthenticationFilter.class)
	        // And filter other requests to check the presence of JWT in header
	        .addFilterBefore(new JwtAuthenticationFilter(),
	                UsernamePasswordAuthenticationFilter.class);
	    
	    http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
	  }

	  @Override
	  protected void configure(AuthenticationManagerBuilder auth) {
		  auth.authenticationProvider(tokenAuthentication);
	  }
	
}
