/**
 * 
 */
package com.it218.AttendanceSystem.security;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.it218.AttendanceSystem.dto.EmployeeDTO;

/**
 * @author reneir.val.t.perez
 *
 */
public class JwtLoginFilter extends AbstractAuthenticationProcessingFilter {
	
	public JwtLoginFilter(String url, AuthenticationManager authManager) {
		super(new AntPathRequestMatcher(url));
	    setAuthenticationManager(authManager);
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) 
			throws AuthenticationException, IOException, ServletException {
		EmployeeDTO creds = new ObjectMapper().readValue(req.getInputStream(), EmployeeDTO.class);
    
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
            creds.getId(),
            creds.getPassword(),
            Collections.emptyList()
		);
		
		return getAuthenticationManager().authenticate(token);
		        
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain, Authentication auth) 
			throws IOException, ServletException {
		TokenAuthenticationService.addAuthentication(res, auth.getName(), auth.getAuthorities());
	}

}
