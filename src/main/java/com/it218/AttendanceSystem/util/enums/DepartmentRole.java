/**
 * 
 */
package com.it218.AttendanceSystem.util.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author reneir.val.t.perez
 *
 */
public enum DepartmentRole {

	ADMINISTRATOR("Administrator"),
	DEPARTMENT_MANAGER("Department Manager"),
	TECHNICAL_MANAGER("Technical Manager"),
	TEAM_LEADER("Team Leader"),
	TEAM_MEMBER("Team Member")
	;


	private String value;	
	
	private DepartmentRole(String value) {
		this.value = value;
	}

	@JsonValue
	public String getValue() {
		return this.value;
	}	
	
}
