package com.it218.AttendanceSystem.util.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @author reneir.val.t.perez
 *
 */
public enum RequestPassStatus {

	PENDING(0),
	APPROVED(1),
	REJECTED(2),
	USED(3)
	;
	
	private static Map<Integer, RequestPassStatus> map = new HashMap<>();
	static {
        for (RequestPassStatus status : RequestPassStatus.values()) {
            map.put(status.value, status);
        }
    }	

	private int value;
	
	private RequestPassStatus(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return this.value;
	}

	public static RequestPassStatus valueOf(int value) {
        return (RequestPassStatus) map.get(value);
    }
	
}
