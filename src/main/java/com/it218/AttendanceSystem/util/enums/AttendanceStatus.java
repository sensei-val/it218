package com.it218.AttendanceSystem.util.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author rvalperez
 *
 */
public enum AttendanceStatus {

	REGULAR("Regular"),
	TARDY("Tardy"),
	UNDER_TIME("Undertime"),
	OVER_TIME("Overtime"),
	ABSENT("Absent")
	;
	
	private String value;
	
	private AttendanceStatus(String value) {
		this.value = value;
	}

	@JsonValue
	public String getValue() {
		return this.value;
	}
}
