/**
 * 
 */
package com.it218.AttendanceSystem.util.mapper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.jooq.maven.postgres.tables.records.AttendanceRecord;
import org.jooq.maven.postgres.tables.records.DepartmentRecord;
import org.jooq.maven.postgres.tables.records.EmployeeRecord;
import org.jooq.maven.postgres.tables.records.RequestPassRecord;
import org.jooq.maven.postgres.tables.records.TeamRecord;

import com.it218.AttendanceSystem.dao.EmployeeDAO;
import com.it218.AttendanceSystem.dto.AttendanceDTO;
import com.it218.AttendanceSystem.dto.DepartmentDTO;
import com.it218.AttendanceSystem.dto.EmployeeDTO;
import com.it218.AttendanceSystem.dto.RequestPassDTO;
import com.it218.AttendanceSystem.dto.TeamDTO;
import com.it218.AttendanceSystem.util.enums.DepartmentRole;
import com.it218.AttendanceSystem.util.enums.RequestPassStatus;

/**
 * @author reneir.val.t.perez
 *
 */
public class ObjectMapper {
	
//	@Autowired
//	private static PasswordEncoder passwordEncoder;
	
	private static EmployeeDAO employeeDao;
	
	public static void setEmployeeDao(EmployeeDAO from) {
		ObjectMapper.employeeDao = from;
	}

	public static DepartmentRecord toDepartmentRecord(DepartmentDTO deptDTO) {
		return new DepartmentRecord(
				deptDTO.getId(), 
				deptDTO.getDeptCode(), 
				deptDTO.getDeptName(), 
				null, 
				null);
	}
	
	public static DepartmentDTO toDepartmentDTO(DepartmentRecord deptInfoRecord, 
			EmployeeRecord deptMgrRecord,
			Optional<EmployeeRecord> techMgr1Record,
			Optional<EmployeeRecord> techMgr2Record,
			Optional<EmployeeRecord> techMgr3Record) {
		DepartmentDTO result = new DepartmentDTO();
		result.setId(deptInfoRecord.getId());
		result.setDeptCode(deptInfoRecord.getDeptCode());
		result.setDeptName(deptInfoRecord.getDeptName());
		
		if(deptMgrRecord != null) {
			result.setDeptManager(ObjectMapper.toEmployeeDTO(deptMgrRecord, deptInfoRecord, DepartmentRole.DEPARTMENT_MANAGER));	
		}
		if (techMgr1Record.isPresent()) {
			result.setTechManager1(ObjectMapper.toEmployeeDTO(techMgr1Record.get(), deptInfoRecord, DepartmentRole.TECHNICAL_MANAGER));
		}
		if (techMgr2Record.isPresent()) {
			result.setTechManager2(ObjectMapper.toEmployeeDTO(techMgr2Record.get(), deptInfoRecord, DepartmentRole.TECHNICAL_MANAGER));
		}
		if (techMgr3Record.isPresent()) {
			result.setTechManager3(ObjectMapper.toEmployeeDTO(techMgr3Record.get(), deptInfoRecord, DepartmentRole.TECHNICAL_MANAGER));
		}
		return result;
	}
	
	public static EmployeeRecord toEmployeeRecord(EmployeeDTO employeeDto) {
		return new EmployeeRecord(
				employeeDto.getId(),
				//passwordEncoder.encode(employeeDto.getPassword()),
				employeeDto.getPassword(),
				employeeDto.getFirstName(),
				employeeDto.getLastName(),
				employeeDto.getAddress(),
				new Timestamp(employeeDto.getBirthday().getTime()),
				employeeDto.getDepartment().getId(),
				employeeDto.getDepartmentRole().name(),
				null,
				null);
				
	}
	
	public static EmployeeDTO toEmployeeDTO(EmployeeRecord employeeRecord, DepartmentRecord departmentRecord, DepartmentRole role) {
		EmployeeDTO result = new EmployeeDTO();
		result.setId(employeeRecord.getId());
		result.setPassword(employeeRecord.getPassword());
		result.setFirstName(employeeRecord.getFirstName());
		result.setLastName(employeeRecord.getLastName());
		result.setAddress(employeeRecord.getAddress());
		result.setBirthday(employeeRecord.getBirthday());
		result.setDepartmentRole(role);
		
		if (departmentRecord != null) {
			DepartmentDTO departmentDto = new DepartmentDTO();
			departmentDto.setId(departmentRecord.getId());
			departmentDto.setDeptCode(departmentRecord.getDeptCode());
			departmentDto.setDeptName(departmentRecord.getDeptName());
	//		departmentDto.setDeptManager(result);
			result.setDepartment(departmentDto);
		}
		return result;				
	}
	
	public static TeamRecord toTeamRecord(TeamDTO teamDTO) {
		return new TeamRecord(
				teamDTO.getId(), 
				teamDTO.getDepartment().getId(), 
				teamDTO.getProjectName(), 
				null, 
				null);
	}
	
	public static TeamDTO toTeamDTO(TeamRecord teamRecord, DepartmentRecord deptInfoRecord, List<EmployeeRecord> members) {
		TeamDTO result = new TeamDTO();
		result.setId(teamRecord.getId());
		result.setProjectName(teamRecord.getProjectName());		
		result.setDepartment(ObjectMapper.toDepartmentDTO(
				deptInfoRecord, null, Optional.empty(), Optional.empty(), Optional.empty()));
		List<EmployeeDTO> membersDto = new ArrayList<>();
		for (EmployeeRecord memberRecord : members) {
			membersDto.add(ObjectMapper.toEmployeeDTO(
					memberRecord, deptInfoRecord, DepartmentRole.valueOf(memberRecord.getDepartmentRole())));
		}
		result.setMembers(membersDto);		
		return result;
	}
		
	public static AttendanceRecord toAttendanceRecord(AttendanceDTO attendanceDto) {
		return new AttendanceRecord(
				attendanceDto.getId(), 
				attendanceDto.getEmployee().getId(),
				Timestamp.valueOf(attendanceDto.getDate().atStartOfDay()),
				Timestamp.valueOf(attendanceDto.getTimein()),
				Timestamp.valueOf(attendanceDto.getTimeout()),
				null, 
				null);
	}
	
	public static AttendanceDTO toAttendanceDTO(AttendanceRecord record, 
			EmployeeRecord employeeRecord, 
			DepartmentRecord deptRecord, 
			DepartmentRole role) {
		AttendanceDTO result = new AttendanceDTO();
		result.setId(record.getId());
		result.setEmployee(ObjectMapper.toEmployeeDTO(employeeRecord, deptRecord, role));
		result.setDate(record.getDate().toLocalDateTime().toLocalDate());
		result.setTimein(record.getTimein().toLocalDateTime());
		if(record.getTimeout() != null) {
			result.setTimeout(record.getTimeout().toLocalDateTime());
		}
		return result;
	}
	
	public static RequestPassRecord toRequestPassRecord(RequestPassDTO requestPassDTO) {
		return new RequestPassRecord(
				requestPassDTO.getId(),
				requestPassDTO.getDate(),
				requestPassDTO.getRequestorId(),
				requestPassDTO.getApproverId(),
				requestPassDTO.getPassStatus().getValue(),
				null, 
				null);
	}
	
	public static RequestPassDTO toRequestPassDTO(RequestPassRecord record) {
		RequestPassDTO result = new RequestPassDTO();
		result.setId(record.getId());
		result.setDate(record.getDate());
		result.setRequestorId(record.getRequestorId());
		result.setRequestorName(employeeDao.findEmployeeName(record.getRequestorId()));
		result.setPassStatus(RequestPassStatus.valueOf(record.getPassStatus()));
		
		if (record.getApproverId() != null) {
			result.setApproverId(record.getApproverId());
			result.setApproverName(employeeDao.findEmployeeName(record.getApproverId()));
		}
		
		return result;
	}
}
