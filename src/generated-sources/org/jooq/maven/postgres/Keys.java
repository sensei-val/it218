/*
 * This file is generated by jOOQ.
*/
package org.jooq.maven.postgres;


import javax.annotation.Generated;

import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.UniqueKey;
import org.jooq.impl.AbstractKeys;
import org.jooq.maven.postgres.tables.Attendance;
import org.jooq.maven.postgres.tables.Department;
import org.jooq.maven.postgres.tables.Employee;
import org.jooq.maven.postgres.tables.RequestPass;
import org.jooq.maven.postgres.tables.Team;
import org.jooq.maven.postgres.tables.TeamEmployee;
import org.jooq.maven.postgres.tables.records.AttendanceRecord;
import org.jooq.maven.postgres.tables.records.DepartmentRecord;
import org.jooq.maven.postgres.tables.records.EmployeeRecord;
import org.jooq.maven.postgres.tables.records.RequestPassRecord;
import org.jooq.maven.postgres.tables.records.TeamEmployeeRecord;
import org.jooq.maven.postgres.tables.records.TeamRecord;


/**
 * A class modelling foreign key relationships between tables of the <code>public</code> 
 * schema
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

    // -------------------------------------------------------------------------
    // IDENTITY definitions
    // -------------------------------------------------------------------------

    public static final Identity<AttendanceRecord, Integer> IDENTITY_ATTENDANCE = Identities0.IDENTITY_ATTENDANCE;
    public static final Identity<DepartmentRecord, Integer> IDENTITY_DEPARTMENT = Identities0.IDENTITY_DEPARTMENT;
    public static final Identity<EmployeeRecord, Integer> IDENTITY_EMPLOYEE = Identities0.IDENTITY_EMPLOYEE;
    public static final Identity<RequestPassRecord, Integer> IDENTITY_REQUEST_PASS = Identities0.IDENTITY_REQUEST_PASS;
    public static final Identity<TeamRecord, Integer> IDENTITY_TEAM = Identities0.IDENTITY_TEAM;

    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    public static final UniqueKey<AttendanceRecord> PK_ATTENDANCE = UniqueKeys0.PK_ATTENDANCE;
    public static final UniqueKey<DepartmentRecord> PK_DEPARTMENT = UniqueKeys0.PK_DEPARTMENT;
    public static final UniqueKey<EmployeeRecord> PK_EMPLOYEE = UniqueKeys0.PK_EMPLOYEE;
    public static final UniqueKey<RequestPassRecord> PK_REQUEST_PASS = UniqueKeys0.PK_REQUEST_PASS;
    public static final UniqueKey<TeamRecord> PK_TEAM = UniqueKeys0.PK_TEAM;
    public static final UniqueKey<TeamEmployeeRecord> PK_TEAM_EMPLOYEE = UniqueKeys0.PK_TEAM_EMPLOYEE;

    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------

    public static final ForeignKey<AttendanceRecord, EmployeeRecord> ATTENDANCE__FK_ATTENDANCE_0 = ForeignKeys0.ATTENDANCE__FK_ATTENDANCE_0;
    public static final ForeignKey<EmployeeRecord, DepartmentRecord> EMPLOYEE__FK_EMPLOYEE_0 = ForeignKeys0.EMPLOYEE__FK_EMPLOYEE_0;
    public static final ForeignKey<RequestPassRecord, EmployeeRecord> REQUEST_PASS__FK_REQUEST_PASS_0 = ForeignKeys0.REQUEST_PASS__FK_REQUEST_PASS_0;
    public static final ForeignKey<RequestPassRecord, EmployeeRecord> REQUEST_PASS__FK_REQUEST_PASS_1 = ForeignKeys0.REQUEST_PASS__FK_REQUEST_PASS_1;
    public static final ForeignKey<TeamRecord, DepartmentRecord> TEAM__FK_TEAM_0 = ForeignKeys0.TEAM__FK_TEAM_0;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Identities0 extends AbstractKeys {
        public static Identity<AttendanceRecord, Integer> IDENTITY_ATTENDANCE = createIdentity(Attendance.ATTENDANCE, Attendance.ATTENDANCE.ID);
        public static Identity<DepartmentRecord, Integer> IDENTITY_DEPARTMENT = createIdentity(Department.DEPARTMENT, Department.DEPARTMENT.ID);
        public static Identity<EmployeeRecord, Integer> IDENTITY_EMPLOYEE = createIdentity(Employee.EMPLOYEE, Employee.EMPLOYEE.ID);
        public static Identity<RequestPassRecord, Integer> IDENTITY_REQUEST_PASS = createIdentity(RequestPass.REQUEST_PASS, RequestPass.REQUEST_PASS.ID);
        public static Identity<TeamRecord, Integer> IDENTITY_TEAM = createIdentity(Team.TEAM, Team.TEAM.ID);
    }

    private static class UniqueKeys0 extends AbstractKeys {
        public static final UniqueKey<AttendanceRecord> PK_ATTENDANCE = createUniqueKey(Attendance.ATTENDANCE, "pk_attendance", Attendance.ATTENDANCE.ID);
        public static final UniqueKey<DepartmentRecord> PK_DEPARTMENT = createUniqueKey(Department.DEPARTMENT, "pk_department", Department.DEPARTMENT.ID);
        public static final UniqueKey<EmployeeRecord> PK_EMPLOYEE = createUniqueKey(Employee.EMPLOYEE, "pk_employee", Employee.EMPLOYEE.ID);
        public static final UniqueKey<RequestPassRecord> PK_REQUEST_PASS = createUniqueKey(RequestPass.REQUEST_PASS, "pk_request_pass", RequestPass.REQUEST_PASS.ID);
        public static final UniqueKey<TeamRecord> PK_TEAM = createUniqueKey(Team.TEAM, "pk_team", Team.TEAM.ID, Team.TEAM.DEPT_ID);
        public static final UniqueKey<TeamEmployeeRecord> PK_TEAM_EMPLOYEE = createUniqueKey(TeamEmployee.TEAM_EMPLOYEE, "pk_team_employee", TeamEmployee.TEAM_EMPLOYEE.TEAM_ID, TeamEmployee.TEAM_EMPLOYEE.MEMBER_ID);
    }

    private static class ForeignKeys0 extends AbstractKeys {
        public static final ForeignKey<AttendanceRecord, EmployeeRecord> ATTENDANCE__FK_ATTENDANCE_0 = createForeignKey(org.jooq.maven.postgres.Keys.PK_EMPLOYEE, Attendance.ATTENDANCE, "attendance__fk_attendance_0", Attendance.ATTENDANCE.EMPLOYEE_ID);
        public static final ForeignKey<EmployeeRecord, DepartmentRecord> EMPLOYEE__FK_EMPLOYEE_0 = createForeignKey(org.jooq.maven.postgres.Keys.PK_DEPARTMENT, Employee.EMPLOYEE, "employee__fk_employee_0", Employee.EMPLOYEE.DEPARTMENT_ID);
        public static final ForeignKey<RequestPassRecord, EmployeeRecord> REQUEST_PASS__FK_REQUEST_PASS_0 = createForeignKey(org.jooq.maven.postgres.Keys.PK_EMPLOYEE, RequestPass.REQUEST_PASS, "request_pass__fk_request_pass_0", RequestPass.REQUEST_PASS.REQUESTOR_ID);
        public static final ForeignKey<RequestPassRecord, EmployeeRecord> REQUEST_PASS__FK_REQUEST_PASS_1 = createForeignKey(org.jooq.maven.postgres.Keys.PK_EMPLOYEE, RequestPass.REQUEST_PASS, "request_pass__fk_request_pass_1", RequestPass.REQUEST_PASS.APPROVER_ID);
        public static final ForeignKey<TeamRecord, DepartmentRecord> TEAM__FK_TEAM_0 = createForeignKey(org.jooq.maven.postgres.Keys.PK_DEPARTMENT, Team.TEAM, "team__fk_team_0", Team.TEAM.DEPT_ID);
    }
}
