/*
 * This file is generated by jOOQ.
*/
package org.jooq.maven.postgres.tables;


import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;
import org.jooq.maven.postgres.Keys;
import org.jooq.maven.postgres.Public;
import org.jooq.maven.postgres.tables.records.RequestPassRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class RequestPass extends TableImpl<RequestPassRecord> {

    private static final long serialVersionUID = -1280950590;

    /**
     * The reference instance of <code>public.request_pass</code>
     */
    public static final RequestPass REQUEST_PASS = new RequestPass();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<RequestPassRecord> getRecordType() {
        return RequestPassRecord.class;
    }

    /**
     * The column <code>public.request_pass.id</code>.
     */
    public final TableField<RequestPassRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("nextval('request_pass_id_seq'::regclass)", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>public.request_pass.date</code>.
     */
    public final TableField<RequestPassRecord, Timestamp> DATE = createField("date", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false), this, "");

    /**
     * The column <code>public.request_pass.requestor_id</code>.
     */
    public final TableField<RequestPassRecord, Integer> REQUESTOR_ID = createField("requestor_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>public.request_pass.approver_id</code>.
     */
    public final TableField<RequestPassRecord, Integer> APPROVER_ID = createField("approver_id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>public.request_pass.pass_status</code>.
     */
    public final TableField<RequestPassRecord, Integer> PASS_STATUS = createField("pass_status", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>public.request_pass.created_date</code>.
     */
    public final TableField<RequestPassRecord, Timestamp> CREATED_DATE = createField("created_date", org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.request_pass.updated_date</code>.
     */
    public final TableField<RequestPassRecord, Timestamp> UPDATED_DATE = createField("updated_date", org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * Create a <code>public.request_pass</code> table reference
     */
    public RequestPass() {
        this("request_pass", null);
    }

    /**
     * Create an aliased <code>public.request_pass</code> table reference
     */
    public RequestPass(String alias) {
        this(alias, REQUEST_PASS);
    }

    private RequestPass(String alias, Table<RequestPassRecord> aliased) {
        this(alias, aliased, null);
    }

    private RequestPass(String alias, Table<RequestPassRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<RequestPassRecord, Integer> getIdentity() {
        return Keys.IDENTITY_REQUEST_PASS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<RequestPassRecord> getPrimaryKey() {
        return Keys.PK_REQUEST_PASS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<RequestPassRecord>> getKeys() {
        return Arrays.<UniqueKey<RequestPassRecord>>asList(Keys.PK_REQUEST_PASS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<RequestPassRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<RequestPassRecord, ?>>asList(Keys.REQUEST_PASS__FK_REQUEST_PASS_0, Keys.REQUEST_PASS__FK_REQUEST_PASS_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RequestPass as(String alias) {
        return new RequestPass(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public RequestPass rename(String name) {
        return new RequestPass(name, null);
    }
}
