/*
 * This file is generated by jOOQ.
*/
package org.jooq.maven.postgres.tables;


import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;
import org.jooq.maven.postgres.Keys;
import org.jooq.maven.postgres.Public;
import org.jooq.maven.postgres.tables.records.TeamRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Team extends TableImpl<TeamRecord> {

    private static final long serialVersionUID = -564680934;

    /**
     * The reference instance of <code>public.team</code>
     */
    public static final Team TEAM = new Team();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TeamRecord> getRecordType() {
        return TeamRecord.class;
    }

    /**
     * The column <code>public.team.id</code>.
     */
    public final TableField<TeamRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("nextval('team_id_seq'::regclass)", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>public.team.dept_id</code>.
     */
    public final TableField<TeamRecord, Integer> DEPT_ID = createField("dept_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>public.team.project_name</code>.
     */
    public final TableField<TeamRecord, String> PROJECT_NAME = createField("project_name", org.jooq.impl.SQLDataType.VARCHAR.length(50), this, "");

    /**
     * The column <code>public.team.created_date</code>.
     */
    public final TableField<TeamRecord, Timestamp> CREATED_DATE = createField("created_date", org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.team.updated_date</code>.
     */
    public final TableField<TeamRecord, Timestamp> UPDATED_DATE = createField("updated_date", org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * Create a <code>public.team</code> table reference
     */
    public Team() {
        this("team", null);
    }

    /**
     * Create an aliased <code>public.team</code> table reference
     */
    public Team(String alias) {
        this(alias, TEAM);
    }

    private Team(String alias, Table<TeamRecord> aliased) {
        this(alias, aliased, null);
    }

    private Team(String alias, Table<TeamRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<TeamRecord, Integer> getIdentity() {
        return Keys.IDENTITY_TEAM;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<TeamRecord> getPrimaryKey() {
        return Keys.PK_TEAM;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<TeamRecord>> getKeys() {
        return Arrays.<UniqueKey<TeamRecord>>asList(Keys.PK_TEAM);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<TeamRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<TeamRecord, ?>>asList(Keys.TEAM__FK_TEAM_0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Team as(String alias) {
        return new Team(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Team rename(String name) {
        return new Team(name, null);
    }
}
